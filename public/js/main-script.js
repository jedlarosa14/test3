$('.panel-heading a').click(function() {
    $('.panel-heading').removeClass('active');
    if(!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
        $(this).parents('.panel-heading').addClass('active');
});

/*
$('.testimonial').slick({
  arrow: true,
  infinite: true,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 2,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
*/
// Mobile Nav
var navTrigger = document.getElementsByClassName('nav-trigger')[0],
    body = document.getElementsByTagName('body')[0];

navTrigger.addEventListener('click', toggleNavigation);

function toggleNavigation(event) {
  event.preventDefault();
  body.classList.toggle('nav-open');
}

jQuery(document).ready(function($)  {
   $('.game_guides .faq_question').click(function() {   
     if ($(this).parent().is('.open')){
       $(this).closest('.faq').find('.faq_answer_container').animate({'height':'0'},500);
       $(this).closest('.faq').removeClass('open');
       $(this).closest('.faq_container').removeClass('open');
       $(this).parent().find('.accordion-button-icon').removeClass('fa-angle-up').addClass('fa-angle-down');
     }
     else{
       var newHeight =$(this).closest('.faq').find('.faq_answer').height() +'px';
       $(this).closest('.faq').find('.faq_answer_container').animate({'height':newHeight},500);
       $(this).closest('.faq').addClass('open');
       $(this).closest('.faq_container').addClass('open');
       $(this).parent().find('.accordion-button-icon').removeClass('fa-angle-down').addClass('fa-angle-up');
    }  
   });
});



jQuery(document).ready(function($)  {
   $('.faq_question').click(function() {   
       $(this).closest('.faq_container').toggleClass('open');
   });
});