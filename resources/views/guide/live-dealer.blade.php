@extends('layouts.app')
@section('title') Guide- Live dealer @endsection
@section('content')

<div class="main-betting-guide">

  <section class="betting-guide dealer-guide position-relative">
    <div class="container">
      <div class="row">
        <div class="col-12  d-none d-xl-block">
          <div class="big_img_banner">
            <img src="{{ asset('images/live-dealer-guide/man-ban-using-phone.png') }}" alt="img">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-xl-6">
          <div class="banner-content">
            <h1>Compare the Top Live Casinos & Games with Real Dealers</h1>
            <p class="sub_title my-4">The Best Live Casinos & Games with Real Dealers </p>
            <p class="white">With live casino games you'll be playing with real dealers, watching them interact with the
              cards and tables via a video stream. Live dealer games provide both the thrills of a land based casino and
              all the perks you'll enjoy at an internet gambling site, including huge real money bonuses, software from
              top developers you can trust like Microgaming and Evolution, and brilliant game variety. If you’re ready
              for the most realistic casino experience online, we’ve shortlisted the top live casino websites offering
              these games below.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="play_money  white">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-7">
          <h2>Play Real Money Online Slots</h2>
          <ul class="list my-4">
            <li>Huge Welcome Bonus Package</li>
            <li>Visa And Mastercard Payment Options Available For Us Players</li>
            <li>Over 150 Real Money Online Slot Games</li>
          </ul>

          <div class="d-flex flex-wrap align-items-center">
            <a class="ylw_play_btn me-5" href="#">Play Real Money Online Slots</a>
            <div class="pay_method">
              <small>Payment Method:</small>
              <ul class="cards_logo d-flex flex-wrap my-2">
                <li><img src="{{ asset('images/live-dealer-guide/visa--light.png') }}" alt="icon"></li>
                <li><img src="{{ asset('images/live-dealer-guide/americanexpress--light.png') }}" alt="icon"></li>
                <li><img src="{{ asset('images/live-dealer-guide/bitcoin--light.png') }}" alt="icon"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="content_box cmn-padd text-center text-lg-start">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-lg-9 order-2 order-lg-0">
          <h2 class="hd-2 with_tick pb-2 font-25 text-uppercase">Live Casinos Online</h2>
          <div class="ylw_bg_text white  my-4"><span class="px-4 py-2 rounded">Roulette is one of the most popular and
              classic casino games.</span></div>
          <p>When you play at a live casino online, you can <strong>interact and socialize with the dealer and other
              players</strong>. It’s a real-world gambling experience from the comfort of home. Everything about the
            games happens in real-time streaming video.</p>

          <p>Let’s take a look at some of <strong>the best live gambling sites</strong>. We’ll show you what games are
            available, how they work and explain some of the features the technology provides.</p>
        </div>

        <div class="col-12 col-lg-3">
          <img src="{{ asset('images/live-dealer-guide/casino-1.png') }}" alt="img">
        </div>
      </div>
    </div>
  </section>


  <section class="casino_slot">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading d-flex align-items-center justify-content-center justify-content-lg-start">
            <img class="me-4" src="{{ asset('images/live-dealer-guide/money.png') }}" alt="money">
            <h2 class="hd-2 mb-0 font-25 text-uppercase">Live Dealer Casinos <i class="d-block">Everything You Need to
                Play Online</i></h2>
          </div>
        </div>
      </div>
      <div class="row my-5">
        <div class="col-12">
          <ul class="row casino_needs g-1">
            <li class="col">
              <div class="img_wrap text-center">
                <img class="mb-3" src="{{ asset('images/live-dealer-guide/award.png') }}" alt="brand">
                <p class="mb-0">Best Live <br> Dealer Games</p>
              </div>
            </li>
            <li class="col">
              <div class="img_wrap text-center">
                <img class="mb-3" src="{{ asset('images/live-dealer-guide/casino.png') }}" alt="brand">
                <p class="mb-0">Best Live <br> Dealer Games</p>
              </div>
            </li>
            <li class="col">
              <div class="img_wrap text-center">
                <img class="mb-3" src="{{ asset('images/live-dealer-guide/diamond.png') }}" alt="brand">
                <p class="mb-0">Best Live <br> Dealer Games</p>
              </div>
            </li>
            <li class="col">
              <div class="img_wrap text-center">
                <img class="mb-3" src="{{ asset('images/live-dealer-guide/bank.png') }}" alt="brand">
                <p class="mb-0">Best Live <br> Dealer Games</p>
              </div>
            </li>
            <li class="col">
              <div class="img_wrap text-center">
                <img class="mb-3" src="{{ asset('images/live-dealer-guide/faq.png') }}" alt="brand">
                <p class="mb-0">Best Live <br> Dealer Games</p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>


  <section class="liveguide-featured-box cmn-padd">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-10 mb-4 mb-md-0">
          <div class="featured_box mb-3">
            <div class="card">
              <h5 class="card-header bold white">Best Casinos to Play Roulette for Real Money 2021</h5>
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-md-6 col-lg-5">
                    <div class="box text-center">
                      <div class="logo">
                        <img src="{{ asset('images/live-dealer-guide/logo_black.3c200844.png') }}" alt="logo">
                      </div>
                      <div class="buttons">
                        <a class="ylw_play_btn grn" href="#">Visit Site and Play Now</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                    <div class="box mt-4 mt-md-0 d-flex align-items-center flex-column d-md-block">
                      <div class="text mb-3 text-center text-md-start">
                        <strong>Las Atlantis</strong>
                        <span>280% UP TO $14,000</span>
                      </div>
                      <ul class="points mb-0">
                        <li>Huge Welcome Bonus Package</li>
                        <li>Visa and MasterCard Payment Options Available for US Players</li>
                        <li>Over 150 Real Money Online Slot Games</li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-12 col-lg-3 mt-4 mt-lg-0">
                    <div class="box d-flex d-lg-block justify-content-center">
                      <div class="pay_method mb-4">
                        <small>Payment Method:</small>
                        <ul class="cards_logo d-flex flex-wrap my-2">
                          <li><img src="{{ asset('images/live-dealer-guide/visa--light.png') }}" alt="icon"></li>
                          <li><img src="{{ asset('images/live-dealer-guide/americanexpress--light.png') }}" alt="icon">
                          </li>
                          <li><img src="{{ asset('images/live-dealer-guide/bitcoin--light.png') }}" alt="icon"></li>
                        </ul>
                      </div>
                      <div class="compatible_with ms-sm-4 ms-lg-0">
                        <small>Compatible with:</small>
                        <ul class="my-2 d-flex flex-wrap">
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-ionic-logo-windows.png') }}" alt="icon">
                          </li>
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-simple-apple.png') }}" alt="icon"></li>
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-metro-android.png') }}" alt="icon"></li>
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-metro-mobile.png') }}" alt="icon"></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="featured_box normal mb-3">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-md-5">
                    <div class="box text-center">
                      <div class="logo">
                        <img src="{{ asset('images/live-dealer-guide/elroyalecasino_logo_250x250.png') }}" alt="logo">
                      </div>
                      <div class="buttons">
                        <a class="ylw_play_btn grn" href="#">Visit Site and Play Now</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-4">
                    <div class="box mt-4 mt-md-0 d-flex align-items-center flex-column d-md-block">
                      <div class="text mb-3 text-center text-md-start">
                        <strong>El Royale Casino</strong>
                        <span>250% UP TO $12,500</span>
                      </div>
                      <ul class="points mb-0">
                        <li>Safe and Reputable Casino, Mobile Friendly</li>
                        <li>Easy Credit Card Deposits for US Players</li>
                        <li>Impressive Welcome Bonuses up to 260% match</li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-12 col-md-3 mt-4 mt-lg-0">
                    <div class="box d-flex d-lg-block justify-content-center">
                      <div class="pay_method mb-4">
                        <small>Payment Method:</small>
                        <ul class="cards_logo d-flex flex-wrap my-2">
                          <li><img src="{{ asset('images/live-dealer-guide/visa--light.png') }}" alt="icon"></li>
                          <li><img src="{{ asset('images/live-dealer-guide/americanexpress--light.png') }}" alt="icon">
                          </li>
                          <li><img src="{{ asset('images/live-dealer-guide/bitcoin--light.png') }}" alt="icon"></li>
                        </ul>
                      </div>
                      <div class="compatible_with ms-sm-4 ms-lg-0">
                        <small>Compatible with:</small>
                        <ul class="my-2 d-flex flex-wrap">
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-ionic-logo-windows.png') }}" alt="icon">
                          </li>
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-simple-apple.png') }}" alt="icon"></li>
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-metro-android.png') }}" alt="icon"></li>
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-metro-mobile.png') }}" alt="icon"></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="featured_box normal">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-md-5">
                    <div class="box text-center">
                      <div class="logo">
                        <img src="{{ asset('images/live-dealer-guide/mybookie.png') }}" alt="logo">
                      </div>
                      <div class="buttons">
                        <a class="ylw_play_btn grn" href="#">Visit Site and Play Now</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-4">
                    <div class="box mt-4 mt-md-0 d-flex align-items-center flex-column d-md-block">
                      <div class="text mb-3">
                        <strong>My Bookie</strong>
                        <span>150% UP TO $750</span>
                      </div>
                      <ul class="points mb-0">
                        <li>USA Players Welcome & Fast Payouts</li>
                        <li>Very Reputable Casino</li>
                        <li>Live Dealer Games Available</li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-12 col-md-3 mt-4 mt-lg-0">
                    <div class="box d-flex d-lg-block justify-content-center">
                      <div class="pay_method mb-4">
                        <small>Payment Method:</small>
                        <ul class="cards_logo d-flex flex-wrap my-2">
                          <li><img src="{{ asset('images/live-dealer-guide/visa--light.png') }}" alt="icon"></li>
                          <li><img src="{{ asset('images/live-dealer-guide/americanexpress--light.png') }}" alt="icon">
                          </li>
                          <li><img src="{{ asset('images/live-dealer-guide/bitcoin--light.png') }}" alt="icon"></li>
                        </ul>
                      </div>
                      <div class="compatible_with ms-sm-4 ms-lg-0">
                        <small>Compatible with:</small>
                        <ul class="my-2 d-flex flex-wrap">
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-ionic-logo-windows.png') }}" alt="icon">
                          </li>
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-simple-apple.png') }}" alt="icon"></li>
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-metro-android.png') }}" alt="icon"></li>
                          <li><img src="{{ asset('images/live-dealer-guide/Icon-metro-mobile.png') }}" alt="icon"></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="text-center mt-3 mt-md-5"><a class="ylw_play_btn grn" href="#">Show More Reviews</a></div>
        </div>
        <div class="col-12 col-lg-2">
          <div class="side_widget">
            <div class="cmn_widget">
              <h3><img src="{{ asset('images/live-dealer-guide/betting.png') }}" alt="icon">Popular Games</h3>
              <ul>
                <li>Online Slots</li>
                <li>Online Blackjack</li>
                <li>Online Scratch Cards</li>
                <li>Online Bingo</li>
                <li>Online Keno</li>
                <li>Online Video Poker</li>
                <li>Online Roulette</li>
                <li>Online Craps</li>
              </ul>
            </div>

            <div class="cmn_widget">
              <h3><img src="{{ asset('images/live-dealer-guide/dices.png') }}" alt="icon">US Banking Options</h3>
              <ul>
                <li>Casino Deposit Methods</li>
                <li>Visa Gift Card</li>
                <li>Bitcoin</li>
                <li>Visa</li>
                <li>Checks</li>
                <li>Online Casino Withdrawals</li>
              </ul>
            </div>

            <div class="cmn_widget">
              <h3><img src="{{ asset('images/live-dealer-guide/dealer.png') }}" alt="icon">Live Dealer Games</h3>
              <ul>
                <li>Live Blackjack</li>
                <li>Live Roulette</li>
                <li>Live Casino Hold’em</li>
                <li>Live Baccarat</li>
                <li>Live Super Six</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="content_box  pb-4">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 mb-4">
          <h2 class="hd-2 with_tick pb-2 font-25 text-uppercase">Best USA Real Money Slots Casinos Online</h2>
          <p>There are hundreds of real money online slots for USA players. In this section, we cover the number of
            casino games available, their average return-to-player (RTP), and the available bonus offers.</p>
        </div>

        <div class="col-12 hf_bar">
          <h2>Sports Betting Strategy</h2>
        </div>
        <p>Check out our full list of gambling reviews to find live dealer sites that accept players from the USA. Look
          for the games you enjoy and incredible bonuses. Below you will find <strong>our list of the top live casinos
            online for US players.</strong></p>

      </div>
    </div>
  </section>


  <section class="live-casino">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="table-responsive">
            <table class="table table-borderless text-center">
              <thead>
                <tr class="white text-uppercase">
                  <th scope="col">Rank</th>
                  <th scope="col">online casino</th>
                  <th scope="col">bonus</th>
                  <th scope="col"># of Games</th>
                  <th scope="col">rtp</th>
                  <th scope="col">start</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="rank">1</td>
                  <td class="online-casino">
                    <small class="d-block text-center">Las Atlantis</small>
                    <img src="{{ asset('images/live-dealer-guide/logo_black.3c200844.png') }}" alt="logo">
                  </td>
                  <td class="bonus">
                    <div class="d-flex justify-content-center">
                      <span class="percent">280% </span>
                      <span>up<br>to</span>
                    </div>
                    <strong>$14,000</strong>
                  </td>
                  <td class="nogames">4</td>
                  <td class="rtp">Baccarat</td>
                  <td class="start"><a class="ylw_play_btn grn" href="#">Play Now</a></td>
                </tr>
              </tbody>

              <tbody class="grey">
                <tr>
                  <td class="rank">2</td>
                  <td class="online-casino">
                    <small class="d-block text-center">El Royale Casino</small>
                    <img src="{{ asset('images/live-dealer-guide/elroyalecasino_logo_250x250.png') }}" alt="logo">
                  </td>
                  <td class="bonus">
                    <div class="d-flex justify-content-center">
                      <span class="percent">250% </span>
                      <span>up<br>to</span>
                    </div>
                    <strong>$12,500</strong>
                  </td>
                  <td class="nogames">6</td>
                  <td class="rtp">Super 6</td>
                  <td class="start"><a class="ylw_play_btn red" href="#">Play Now</a></td>
                </tr>
              </tbody>

              <tbody class="grey">
                <tr>
                  <td class="rank">3</td>
                  <td class="online-casino">
                    <small class="d-block text-center">Super Slots</small>
                    <img src="{{ asset('images/live-dealer-guide/ID3423-500x500.png') }}" alt="logo">
                  </td>
                  <td class="bonus">
                    <div class="d-flex justify-content-center">
                      <span class="percent">300% </span>
                      <span>up<br>to</span>
                    </div>
                    <strong>$6,000</strong>
                  </td>
                  <td class="nogames">4</td>
                  <td class="rtp">European Roulette</td>
                  <td class="start"><a class="ylw_play_btn red" href="#">Play Now</a></td>
                </tr>
              </tbody>

              <tbody class="grey">
                <tr>
                  <td class="rank">4</td>
                  <td class="online-casino">
                    <small class="d-block text-center">Slots Empire</small>
                    <img src="{{ asset('images/live-dealer-guide/Slots-Empire-Casino_logo_250x250.png') }}" alt="logo">
                  </td>
                  <td class="bonus">
                    <div class="d-flex justify-content-center">
                      <span class="percent">300% </span>
                      <span>up<br>to</span>
                    </div>
                    <strong>$6,000</strong>
                  </td>
                  <td class="nogames">5</td>
                  <td class="rtp">Blackjack</td>
                  <td class="start"><a class="ylw_play_btn red" href="#">Play Now</a></td>
                </tr>
              </tbody>

              <tbody class="grey">
                <tr>
                  <td class="rank">5</td>
                  <td class="online-casino">
                    <small class="d-block text-center">Wild Casino</small>
                    <img src="{{ asset('images/live-dealer-guide/WildCasino-500x500.png') }}" alt="logo">
                  </td>
                  <td class="bonus">
                    <div class="d-flex justify-content-center">
                      <span class="percent">100% </span>
                      <span>up<br>to</span>
                    </div>
                    <strong>$5,000</strong>
                  </td>
                  <td class="nogames">6</td>
                  <td class="rtp">Casino Hold'em</td>
                  <td class="start"><a class="ylw_play_btn red" href="#">Play Now</a></td>
                </tr>
              </tbody>

            </table>
          </div>

        </div>
      </div>
    </div>
  </section>


  <section class="light-grn-bg cmn-padd my-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="ylw_bg_text white  my-4"><span class="px-4 py-2 rounded">Live Dealer Games</span></div>
          <h2 class="hd-2 with_tick pb-2 font-25 text-uppercase">Live Casino: <strong>A COMPREHENSIVE GUIDE TO LIVE
              DEALER GAMES</strong></h2>
          <p>Despite the best efforts of online casinos to prove the fairness of their RNG (random number generator)
            games, skeptics will probably never be satisfied that game outcomes are truly fair and random. Live casinos
            and live dealer casino games are the natural evolution in winning over the skeptics and providing an
            enhanced online gambling experience.</p>

          <p>As internet connection speeds and live streaming technologies advance, so too does the quality of games
            developed by the leading platforms in this niche.</p>

          <p>Live streamed video with real dealers dealing real cards and launching real balls is the closest thing you
            can get to a traditional ‘bricks and mortar’ casino experience while playing online. Play blackjack,
            roulette, baccarat, poker or sicbo for real money; with the action viewed on high definition real-time video
            streamed to your PC or mobile device.</p>

          <p>Looking to play? Perhaps start at our game summary pages listed below, or if you’re more visually inclined,
            the below game gallery may be helpful.</p>

          Alternatively, try our live casino comparison page for a complete list of live casinos, and their key
          attributes. </p>
        </div>
      </div>
    </div>
  </section>


  <section class="content_box cmn-padd">
    <div class="container">
      <div class="row align-items-center">

        <div class="col-12 hf_bar">
          <h2>How to Play Live Casino Games</h2>
        </div>
        <p>Getting started with live dealer games is really easy, even if you’ve never played at an online casino
          before, and you can be up and running in a matter of minutes:</p>

        <ul class="numb_back row g-2">
          <li class="col">
            <div class="img_wrap">
              <img src="{{ asset('images/live-dealer-guide/web.png') }}" alt="icon">
              <p>Sign up with one of our recommended online casinos and claim your bonus.</p>
            </div>
          </li>
          <li class="col">
            <div class="img_wrap">
              <img src="{{ asset('images/live-dealer-guide/credit-card.png') }}" alt="icon">
              <p>Sign up with one of our recommended online casinos and claim your bonus.</p>
            </div>
          </li>
          <li class="col">
            <div class="img_wrap">
              <img src="{{ asset('images/live-dealer-guide/dealer.png') }}" alt="icon">
              <p>Sign up with one of our recommended online casinos and claim your bonus.</p>
            </div>
          </li>
          <li class="col">
            <div class="img_wrap">
              <img src="{{ asset('images/live-dealer-guide/book.png') }}" alt="icon">
              <p>Sign up with one of our recommended online casinos and claim your bonus.</p>
            </div>
          </li>
          <li class="col">
            <div class="img_wrap">
              <img src="{{ asset('images/live-dealer-guide/chat.png') }}" alt="icon">
              <p>Sign up with one of our recommended online casinos and claim your bonus.</p>
            </div>
          </li>
        </ul>

      </div>
    </div>
  </section>


  <section class="main_dealer_vs">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 with_tick pb-2 font-25 text-uppercase">Live Dealer & Regular Online Casino Games Compared</h2>
          <p class="my-4">Typical online games powered by Random Number Generators (RNGs) and live dealer games both
            come with their own advantages and disadvantages. Your style of play will likely dictate which ends up being
            your favorite, but if you're unsure where to start, take a look at our comparison below:</p>
          <div class="dealer_vs">
            <div class="row">
              <div class="col">
                <div class="ylw_bg_text white mb-4"><span class="px-4 py-2 rounded">Live Dealer Games</span></div>
                <ul class="points mb-0">
                  <li class="right">Enjoy all of the sights and sounds of a brick and mortar casino without leaving your
                    living room</li>
                  <li class="right">Play alongside other players from all over the world, enjoying some camaraderie with
                    them and the dealer</li>
                  <li class="right">Offers a nice bridge for offline gamblers who are put off by the usual mechanics of
                    web casino games</li>
                  <li class="wrong">Games must be played at the pace dictated by the dealer, which might be faster or
                    slower than a player would otherwise prefer</li>
                  <li class="wrong">The range of games available is often much smaller than that of standard online
                    casino games</li>
                </ul>
              </div>

              <div class="col mid-vs"> <span>VS</span></div>

              <div class="col">
                <div class="ylw_bg_text white mb-4"><span class="px-4 py-2 rounded">Regular Casino Games</span></div>
                <ul class="points mb-0">
                  <li class="right">Massive range of games available, often in the hundreds, vs. just a few live dealer
                    games on offer in typical casinos</li>
                  <li class="right">The difference between minimum and maximum possible bets tends to be much higher
                    when playing standard games</li>
                  <li class="right">More diverse bonuses and promotions available to players enjoying standard online
                    casino games</li>
                  <li class="wrong">Possible to burn through your bankroll in a short space of time if you get caught up
                    in the action and bet too quickly</li>
                  <li class="wrong">The action can feel a bit sterile or artificial when compared with the interactivity
                    of a “real” casino</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="content_box cmn-padd pt-0">
    <div class="container">

      <div class="row align-items-center mb-4">
        <div class="col-12 mb-4">
          <h2 class="hd-2 with_tick pb-2 font-25 text-uppercase">What are Live Casinos?</h2>
          <p>Live casinos are a type of online casino that offer a real ‘live’ dealer to host the games. Many people
            find the action at a live casino replicates the experience in a land-based casino best. This makes live
            casinos popular with those who enjoy offline gambling but aren’t able to make the trip to their local
            casino. Some just prefer to play in the comfort of their own home.</p>
        </div>
      </div>

      <div class="row align-items-center">
        <div class="col-12">
          <h2 class="hd-2 with_tick pb-2 font-25 text-uppercase">How do Live Casinos Work?</h2>
          <p>Live casinos function in a similar way to regular online casinos, but the games have a live element. On the
            player’s end, the interface of a live casino looks very similar, with bets placed by pressing buttons in the
            usual way. The big difference is that in a live dealer game, a real ‘live’ dealer (using a real roulette
            wheel, cards etc.) determines the outcome of each round rather than a Random Number Generator. The dealer,
            who is broadcast video a video stream, can interact with players to create a more social atmosphere.</p>
        </div>
      </div>

    </div>
  </section>

  <section class="app-dealer content_box  position-relative">
    <div class="container">

      <div class="row align-items-end">
        <div class="col-12 pb-5 col-lg-8">
          <div class="pb-0 pb-sm-4">
            <h2 class="hd-2 with_tick pb-5 font-25 text-uppercase">Apps & Mobile Dealer</h2>
            <div class="ylw_bg_text white mb-3"><span class="px-4 py-2 rounded">Mobile</span></div>
            <p>If you're looking to play on the go, mobile live dealer games offer an amazing gambling experience. In
              the case of instant play live dealer games, there’s no need to download any extra software to your
              smartphone or tablet because you can access them using a mobile version of the casino’s main site. With
              that being the case, it’s really easy to get started and the only difference between using a mobile device
              and a computer is that graphics etc. will have been scaled back to reduce the strain on your connection.
            </p>

            <div class="ylw_bg_text white mb-3"><span class="px-4 py-2 rounded">Apps</span></div>
            <p>You’ll also find that some live casinos offer dedicated apps for Android, iPhone and iPad in their
              respective app stores. There’s a certain degree of convenience in being able to access your favorite
              casinos and games right from your home screen but, this aside, games look and perform very similarly on
              apps and mobile instant play websites. It's just a matter of which suits you best! </p>
          </div>
        </div>

        <div class="col-12 col-lg-4">
          <div class="image">
            <img class="w-100" src="{{ asset('images/live-dealer-guide/asda.png') }}" alt="image">
          </div>
        </div>
      </div>

    </div>
  </section>


  <section class="game-types cmn-padd">
    <div class="container position-relative">
      <div class="row">
        <div class="col-12 white">
          <h2 class="hd-2 with_tick pb-3 font-25 text-uppercase">Different Live Dealer Game Types</h2>
          <p>You might be disappointed to learn that typical live dealer casinos offer just a handful of base games.
            What might soften that blow, however, is the knowledge that most have many different variants of those core
            games available. From online blackjack to roulette, the most common game variants are shown below:</p>
        </div>
      </div>

      <div class="row mt-5">
        <div class="col-12">
          <div class="table-responsive">
            <table class="table table-bordered text-center bg-white">
              <thead class="white">
                <th>Live Casino Game</th>
                <th>Game Variants</th>
                <th>Play At</th>
              </thead>
              <tbody>
                <tr>
                  <td><strong>Hold’em</strong></td>
                  <td>Casino Hold ‘Em, Ultimate Texas Hold ‘Em, Live Hold ‘Em Pro </td>
                  <td>Spin Casino </td>
                </tr>
                <tr>
                  <td><strong>Baccarat</strong></td>
                  <td>Squeeze, Control Squeeze, Speed, Mini, Punto Banco </td>
                  <td>Ruby Fortune</td>
                </tr>
                <tr>
                  <td><strong>Blackjack</strong></td>
                  <td>VIP, Party, Common Draw, Bet Behind, Perfect Pairs, Pre-Decision</td>
                  <td>Gaming Club</td>
                </tr>
                <tr>
                  <td><strong>Roulette</strong></td>
                  <td>European, French, American, Immersive, Speed, La Partage, Double Ball, Double Wheel, Ra, Dual
                    Play, Dragonara Slingshot, Golden Ball, Sizzling Hot, Auto</td>
                  <td>Royal Vegas</td>
                </tr>
                <tr>
                  <td><strong>Sic Bo</strong></td>
                  <td>Grand Hazard, Chuck-A-Luck</td>
                  <td>All Slots</td>
                </tr>
                <tr>
                  <td><strong>Omaha Hi-Lo</strong></td>
                  <td>Omaha Hi</td>
                  <td>Jackpot City</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="content_box cmn-padd pb-0">
    <div class="container">
      <div class="row align-items-center">

        <div class="col-12">
          <div class="hf_bar_text">
            <h2>Play at a Live Dealer Casino</h2>
            <p>Our reviewers have searched high and low to find sites offering the latest and greatest live dealer
              casino games. No matter what your game and platform of choice, you're sure to find something to suit you,
              so check out our top online casinos with live games and see for yourself!</p>
          </div>
        </div>

      </div>
    </div>
  </section>
  <div class="main_faq pt-3 pb-5 py-md-5">
    <div class="container">
      <div class="row">
        <div class="col-12">

          <h2 class="hd-2 with_tick mb-4">Frequently Asked Questions</h2>
          <div class="faq_container open">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="true"
                aria-controls="collapseExample">
                <span class="question">What are live dealer games?</span>
              </div>

              <div class="faq_answer_container collapse show" id="collapse1">
                <div class="faq_answer"><span>
                    <p>Live dealer games are versions of online casino games that allow the player to connect to a real
                      human dealer via a live video feed. They are available at the most top casino websites and they
                      can be accessed from a computer, mobile, or tablet. Real cards, chips, and roulette wheels are
                      used on the croupier’s end while software to make wagers and bets are used on the player’s end.
                      The live chat options brings a social element to the game. It’s the most realistic casino
                      experience outside of a land-based casino.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">What table games will I find at live casinos?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse2">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Which live dealer games are the most popular?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse3">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Can I try live dealer games for free first?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse4">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse5" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Are live games rigged? </span>
              </div>

              <div class="faq_answer_container collapse" id="collapse5">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>


</div>
@endsection