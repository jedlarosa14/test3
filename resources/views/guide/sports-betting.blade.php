@extends('layouts.app')
@section('title') Guide- Slots Gambling @endsection
@section('content')
<div class="main-betting-guide">

  <section class="betting-guide sport-guide">
    <div class="container">
      <div class="row">
        <div class="col-12 position-relative d-none d-xl-block">
          <div class="big_img_banner">
            <img src="{{ asset('images/sports-betting-guide/Group-194.png') }}" alt="img">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-xl-6">
          <div class="banner-content">
            <h1>Sports Betting Guide –<br> How to Bet on Sports Online</h1>
            <p class="sub_title my-4">While sports betting has a checkered past in the United States, it seems to
              finally be coming out of the shadows. </p>
            <p class="white">Several states have moved to legalize and regulate sports betting since the Supreme Court
              made it legal for them to do so back in 2018. While more states are expected to do the same in the years
              ahead, you don’t have to live in a state with a regulated industry to enjoy some online sports betting.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="content_box beg-guide cmn-padd">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="shadow_box">
            <div class="row">
              <div class="col-12 col-xl-7">
                <div class="content">
                  <h2 class="hd-2">The Comprehensive Beginner’s Guide to Sports Betting</h2>
                  <p class="my-4">Anyone has the potential to be a sharp sports bettor with the right info. That’s why
                    SBD put all the fundamentals into our introductory series for new bettors: Sports Betting 101.<br>
                    Learn about the types of sports bets you can place, how to read odds, and how to manage your
                    bankroll to get the most value out of your wagers.<br>
                    We’ll break down some confusing terms you might encounter while getting started and walk you through
                    some basic considerations to think about before placing a sports bet online.</p>
                  <a href="#" class="ylw_grad_btn">Let's Get Started</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="featured_box cmn-padd pt-0">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <h5 class="card-header">Featured Betting Sites For January 2021</h5>
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col col-xl-2 box">
                  <div class="number">1</div>
                </div>
                <div class="col text-center box">
                  <div class="logo"><img src="{{ asset('images/sports-betting-guide/unnamed.png') }}" alt="logo"></div>
                </div>
                <div class="col d-flex justify-content-center box">
                  <div class="text">
                    <strong>22Bet</strong>
                    <span>100% UP TO $300</span>
                  </div>
                </div>
                <div class="col text-center box mt-4 mt-lg-0">
                  <div class="buttons">
                    <a class="ylw_play_btn mb-3" href="#">Visit Site and Play Now</a>
                    <a class="ylw_play_btn grn" href="#">Review <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="content_box cmn-padd pt-0">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 with_tick pb-2">Sports Betting Guide: How to Bet On Sports For Fun & Profit</h2>
          <div class="ylw_bg_text white  my-4"><span class="px-4 py-2 rounded">Do you realize that sports betting has
              been going on for centuries?</span></div>
          <p>For as long as there have been people playing sports, there have also been people betting on them. The
            reasons why people bet on sports are always the same; betting on sports is fun and there’s a chance of
            winning money.</p>

          <p>Of course, it wasn’t always quite as easy to get your money down as it is today. We didn’t used to have the
            option to bet online. Thankfully, these days we all have access to top sports betting sites like the ones
            you see here.</p>
        </div>
      </div>
    </div>
  </section>


  <section class="black_boxes cmn-padd pt-0">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-4">
          <div class="main_black_box p-5 white">
            <img class="mb-3" src="{{ asset('images/sports-betting-guide/betting-white.png') }}" alt="icon">
            <h3>Betting 101</h3>
            <p>Get started with online sports betting by using our beginners’ guide. Learn how to place wagers and get
              educated on the types of bets you’ll find at top U.S. online sportsbooks. We’ll even show you how to hunt
              for value when betting on your favorite sports.</p>

            <p class="mb-2"><strong>Most popular Betting 101 guides:</strong><br></p>
            <p>A Comprehensive Guide to MLB Totals
              10 Types of Sports Bets Explained
              Guide to Live Sports Betting</p>
            <a href="#" class="ylw_grad_btn mt-3">View All Betting 101</a>
          </div>
        </div>

        <div class="col-12 col-lg-4">
          <div class="main_black_box p-5 white">
            <img class="mb-3" src="{{ asset('images/sports-betting-guide/betting-white.png') }}" alt="icon">
            <h3>Betting Strategy</h3>
            <p>Get ahead of the game with our expert betting strategies. Learn how to master the parlay, win at point
              spreads and make more with in-play betting on NFL, NBA, NHL, college basketball and football, and the MLB.
              We’re your one-stop shop for an online sports betting masterclass.</p>

            <p class="mb-2"><strong>Most popular Betting Strategy guides:</strong><br></p>
            <p>NBA Coach of the Year Betting Tips & Factors to Consider
              NBA Sixth Man of the Year Betting Tips & Advice to Consider
              Tips to Consider When Betting NBA Most Improved Player</p>
            <a href="#" class="ylw_grad_btn mt-3">View All Betting Strategy</a>
          </div>
        </div>

        <div class="col-12 col-lg-4">
          <div class="main_black_box p-5 white">
            <img class="mb-3" src="{{ asset('images/sports-betting-guide/betting-white.png') }}" alt="icon">
            <h3>Banking</h3>
            <p>Learn how to make easy deposits and withdrawals at the best legal and licensed sports betting sites in
              your state! We’ll show you how to choose the right banking method and how to avoid fees and long waiting
              times to start playing – and cashing out.</p>

            <p class="mb-2"><strong>Most popular Banking guides:</strong><br></p>
            <p>Mastercard Betting Sites
              American Express Sports Betting Sites
              Visa Betting Sites</p>
            <a href="#" class="ylw_grad_btn mt-3">View All Betting Strategy</a>
          </div>
        </div>
      </div>
  </section>

  <section class="content_box cmn-padd pt-0">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 with_tick pb-2">Understanding Online Sports Betting</h2>
          <div class="ylw_bg_text white  my-4"><span class="px-4 py-2 rounded">Welcome to USGamblingList.com:</span>
          </div>
          <p><b>Your one-stop shop</b> for expert sports betting advice.</p>

          <p>While you may have heard of terms such as “fade,” “juice” and “ATS trends,” you may not understand what
            they mean and how they can help you win your bet.</p>

          <p>Here, we’ll cover all the basics and even give you a few more advanced strategies so you will have an
            advantage on game day. </p>

          <p>So, take a look around! Learn money management practices that will help preserve your bankroll, understand
            when the moneyline is a better deal than the point spread, and discover how you can save money by shopping
            for the best line.</p>
        </div>
      </div>
    </div>
  </section>

  <section class="content_box cmn-padd pt-0">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 with_tick pb-2">Grow Your Sports Betting Knowledge with Bookies.com</h2>
          <div class="ylw_bg_text white  my-4"><span class="px-4 py-2 rounded">Sports betting is one of the fastest
              growing gambling markets in the US</span></div>
          <p>You can now bet legally on dozens of sports through your PC or mobile device. It’s important, then, that
            you have a firm grasp of how to bet and make the biggest profits.</p>
          <p><b>At USGamblingList.com</b>, we have an unrivaled selection of guides to get you started. We’ll show you
            how to open a sports betting account, make a deposit and place your first wager. We’ll also guide you around
            the intricacies of using parlays and multipliers to improve your long-term results.</p>

          <p>It’s easy to open a sportsbook account, deposit money and place a bet. But it’s harder to find value, make
            the right bets and hold on to your bankroll. That’s why we recommend you read our expert guides before you
            start out.</p>

          <p>Whether you want to bet on the winner of the Super Bowl or the weekend’s college basketball action, we’ll
            show you what to do. And if you’re an experienced gambler, we have some in-depth guides straight from the
            experts. Start your online sports betting journey the right way with Bookies.com. Find the top online
            sportsbooks, unlock great bonuses and promotions and discover the best odds every day.</p>
        </div>
      </div>
    </div>
  </section>


  <section class="content_box pt-0 pb-4">
    <div class="container">
      <div class="row">
        <div class="col-12 hf_bar">
          <h2>Sports Betting Strategy</h2>
        </div>
        <div class="col-12">

          <p>Another very popular section of The Sports Geek is our Sports Betting Strategy section.</p>

          <p>We give you basic sports betting tips and strategies for those bettors new to sports betting, but we also
            have very in-depth sports betting strategy articles written by professional sports bettors, who have made
            hundreds of thousands of dollars betting on sports.</p>

          <p>If you want to make more money betting on sports this is a section of The Sports Geek that you must spend
            some time reading through. The basics will help you get started on the right track, and the advanced
            articles will help you find edges over the sportsbooks and give you the best chance at winning.</p>

          <p>All of this has been done to accomplish our goal of making this the best sports betting guide on the
            Internet. We hope you take the time to read through these articles.</p>

          <div class="ylw_bg_text white my-5"><span class="px-4 py-2 rounded">Here are a few to get you started</span>
          </div>
          <ul class="dark-grn-tick d-flex flex-wrap mb-0">
            <li>How To Win at Sports Betting</li>
            <li>Teaser Betting Strategy</li>
            <li>Sports Betting Math</li>
            <li>Prop Betting Strategy</li>
            <li>How To Get Max Value When Betting</li>
            <li>Future Betting Strategy</li>
            <li>NFL Bye Week Betting Strategy</li>
            <li>Parlay Betting Strategy</li>
            <li>Pleaser Betting Strategy</li>
            <li>Sweetheart Teaser Betting Strategy</li>
            <li>Current Betting Market</li>
          </ul>
        </div>
      </div>
    </div>
  </section>


  <section class="content_box cmn-padd pt-0">
    <div class="container">
      <div class="row">
        <div class="col-12 hf_bar">
          <h2>Sports Betting Odds</h2>
        </div>
        <div class="col-12">
          <p>This section of our website has two purposes. The first is to help beginner sports bettors understand how
            to read betting odds, including learning about the different types of odds you will see posted by the
            sportsbooks.</p>

          <p>The second is to provide live odds feed for bettors to compare the odds some of the top sports betting
            sites are offering. As any experienced sports bettor knows, it is extremely important to compare odds and
            place your wagers using the best odds available to you. This is also referred to as “line shopping”. If you
            read any of our sports betting strategy articles you will know that this is a must if you want to make money
            betting on sports.</p>

          <p>Visit this section today, and bookmark it for future use.</p>
          <a href="#" class="ylw_grad_btn mt-3">Sports Betting Odds</a>
        </div>
      </div>
    </div>
  </section>


  <section class="content_box cmn-padd pt-0">
    <div class="container">
      <div class="row">
        <div class="col-12 hf_bar">
          <h2>Online Sports Betting</h2>
        </div>
        <div class="col-12 mb-5">
          <p>Online sports betting is a billion dollar industry, and it’s becoming more and more each and every day.
            We’ve personally reviewed a number of sports betting sites so that we can recommend only the best sites out
            there.</p>

          <a href="#" class="ylw_grad_btn">Sports Betting Odds</a>
        </div>

        <div class="col-12 mb-5">
          <p>We will say that online sports betting may not be legal where you live, so please check your local laws
            before placing bets online at these sports betting sites. And we also want to point out that there have been
            “scam” type sites in the past known to steal money from their bettors, so make sure you stick with
            recommended betting sites we list here at The Sports Geek.</p>

          <a href="#" class="ylw_grad_btn">Top Sports Betting Sites</a>
        </div>

        <div class="col-12">
          <p>Lastly with online betting it is easy to get carried away and bet more than you can afford. Always make
            sure you gamble responsibly. If you have a gambling problem or know someone who does, please get help. Visit
            Gamblers Anonymous today to learn more about getting help with your sports betting or gambling problem.</p>
        </div>

      </div>
    </div>
  </section>

  <section class="content_box cmn-padd pt-0">
    <div class="container">
      <div class="row">
        <div class="col-12 hf_bar">
          <h2>Find the Right Sports Betting Site</h2>
        </div>
        <div class="col-12">
          <p>The list of pros that comes with betting on sports online is quite a bit longer than the list of cons.
            There are a few things you should be looking for in a betting site before you sign up and start betting,
            though. Things like comprehensive betting coverage of the sport in which you want to bet, competitive online
            betting odds, fast payouts, a variety of banking options, quick withdrawal speeds, and attractive bonuses
            are just a few of the tenets that make the real money online betting process superior to the traditional
            version.</p>

          <p>The following sports betting sites check every single box when it comes to what you should be looking for
            in your online experience:</p>
          <ul class="read_reviews row g-2 row-cols-1 row-cols-sm-2 row-cols-lg-5">
            <li class="col">
              <div class="img_wrap">
                <img src="{{ asset('images/sports-betting-guide/bovada.png') }}" alt="img">
              </div>
              <a href="#" class="ylw_grad_btn">Read Review</a>
            </li>

            <li class="col">
              <div class="img_wrap">
                <img src="{{ asset('images/sports-betting-guide/unnamed.png') }}" alt="img">
              </div>
              <a href="#" class="ylw_grad_btn">Read Review</a>
            </li>

            <li class="col">
              <div class="img_wrap">
                <img src="{{ asset('images/sports-betting-guide/mybookie.png') }}" alt="img">
              </div>
              <a href="#" class="ylw_grad_btn">Read Review</a>
            </li>

            <li class="col">
              <div class="img_wrap">
                <img src="{{ asset('images/sports-betting-guide/13.png') }}" alt="img">
              </div>
              <a href="#" class="ylw_grad_btn">Read Review</a>
            </li>

            <li class="col">
              <div class="img_wrap">
                <img src="{{ asset('images/sports-betting-guide/NoPath - Copy (2).png') }}" alt="img">
              </div>
              <a href="#" class="ylw_grad_btn">Read Review</a>
            </li>
          </ul>

        </div>
      </div>
  </section>

  <section class="content_box pb-0 pt-0 pb-md-4">
    <div class="container">
      <div class="row">
        <div class="col-12 hf_bar">
          <h2>Sports Betting Strategy</h2>
        </div>
        <div class="col-12">

          <p>If you’re looking for somewhere to bet on these sports online, we’ve got you covered. Below we’ve included
            a list of pages on the best online betting sites for the most popular sports to bet on in the world!</p>

          <ul class="dark-grn-tick d-flex flex-wrap mb-0">
            <li>NFL</li>
            <li>NBA</li>
            <li>Soccer</li>
            <li>Baseball</li>
            <li>UFC/MMA</li>
            <li>Boxing</li>
            <li>Tennis</li>
            <li>Hockey</li>
            <li>Golf</li>
            <li>NASCAR</li>
            <li>Table Tennis</li>
            <li>Lacrosse</li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <div class="main_faq pt-3 pb-5 py-md-5">
    <div class="container">
      <div class="row">
        <div class="col-12">

          <h2 class="hd-2 with_tick mb-4">Sports Betting FAQ</h2>
          <div class="faq_container open">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="true"
                aria-controls="collapseExample">
                <span class="question">Is Sports Betting Online Legal?</span>
              </div>

              <div class="faq_answer_container collapse show" id="collapse1">
                <div class="faq_answer"><span>
                    <p>As mentioned, the US Supreme Court made it legal for states to operate their own sports betting
                      industries back in 2018. While many states still have unregulated markets, there is nothing
                      preventing Americans in those states from placing real money sports bets online without having to
                      worry about negative legal repercussions.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Is It Possible to Win Real Money Betting on Sports?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse2">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">How Much Money Can I Win Sports Betting? </span>
              </div>

              <div class="faq_answer_container collapse" id="collapse3">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Can I Bet on Sports Online Safely? </span>
              </div>

              <div class="faq_answer_container collapse" id="collapse4">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse5" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Is Online Sports Betting Safe? </span>
              </div>

              <div class="faq_answer_container collapse" id="collapse5">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse6" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">How Do I Get Started Sports Betting for Real Money?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse6">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse7" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">How Do I Get Started Sports Betting for Real Money?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse7">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse8" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Where Can I Bet on Sports Online?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse8">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse9" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Why Is Online Sports Gambling So Popular?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse9">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse10" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Can I Get Addicted to Sports Betting?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse10">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>

</div>
@endsection