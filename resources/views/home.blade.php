@extends('layouts.app')
@section('title') Home page @endsection
@section('content')
<div class="hm-banner common-banner text-center text-md-start">
<div class="container">
<div class="row">
<div class="col-12 col-lg-7">
  <div class="banner-inner">
  <h1>Your Guide to the World's Best Online Gambling Sites</h1>
  <h3>Best Online Gambling Sites in the US</h3>
  <p>Legal and Social US online gambling sites: sports betting and casino. Exclusive reporting, site reviews and news by gambling.com experts including exclusive bonus offers.</p>
  </div>
</div>
</div> 
</div>
</div>


<div class="gamb_sites cmn-padd">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="hash-sell text-center text-md-start">
            <h2>#1 Best online gambling sites usa</h2>
            <p>Our in-depth reviews and ratings pinpoint the top gambling sites. With 50+ years of sports betting experience, we’ve placed bets at almost every sportsbook online.</p>
        </div>
        <div class="rec_box mb-3 mb-lg-5 text-center text-md-start">
          <h3>Our Top Recommended</h3>
          <p>At USGamblingList.com, we've shortlisted the safest and the best gambling sites for players to gamble online for real money. Enjoy huge bonuses, 100s of games, and play on mobile!</p>
        </div>
        </div>
      </div>

    <div class="row">
      <div class="col-12">
        <ul>
          <li class="gamb_box">
            <div class="left_side">
              <div class="site_logo">
                <div class="img_box"><img src="{{ asset('images/bovada.png') }}" alt="logo"></div>
                <a class="visit_site semi-bold" href="#">Visit Site</a>
              </div>
            </div>

            <div class="right_side">
              <ul class="main_bonus_box">
                <li class="bonus_box dark semi-bold">
                  <span class="small">Bonus</span>
                  <span class="yellow title">100% up to $250</span>
                  <span class="condition">Terms &amp; Conditions Apply</span>
                </li>                
                <li class="bonus_box semi-bold">
                  <span class="small">Win Rate</span>
                  <span class="title">99.09%</span>
                </li>                
                <li class="bonus_box semi-bold">
                  <span class="small">Bonus</span>
                  <span class="title">99.09%</span>
                </li>
              </ul>


              <div class="row">

                <div class="col-12 col-md-6">
                  <div class="pros semi-bold">
                    <h3>Pros</h3>
                    <ul>
                        <li>Easy banking methods</li>
                        <li>Compatible with mobile devices</li>
                        <li>International players welcome </li>                 
                    </ul>
                  </div>                  
                </div>                

                <div class="col-12 col-md-6">
                  <div class="review">
                    <span class="small semi-bold">Deposit Methods &amp; Compatibility</span>
                    <ul class="cards_logo d-flex flex-wrap my-2">
                      <li><img src="{{ asset('images/visa-light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/mastercard--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/neteller--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/bitcoin--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/paynearme--light.png') }}" alt="visa"></li>
                    </ul>
                    <div class="star_review_box d-flex align-items-center">
                    <ul class="star_rate d-flex yellow">
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                    <span><strong class="semi-bold">5</strong>/5</span>
                    </div>
                    <a class="read_review semi-bold" href="#">Read Review</a>
                  </div>                  
                </div>





              </div>

            </div>
          </li>

          <li class="gamb_box">
            <div class="left_side">
              <div class="site_logo">
                <div class="img_box"><img src="{{ asset('images/ignition.png') }}" alt="logo"></div>
                <a class="visit_site semi-bold" href="#">Visit Site</a>
              </div>
            </div>

            <div class="right_side">
              <ul class="main_bonus_box">
                <li class="bonus_box dark semi-bold">
                  <span class="small">Bonus</span>
                  <span class="yellow title">100% up to $250</span>
                  <span class="condition">Terms &amp; Conditions Apply</span>
                </li>                
                <li class="bonus_box semi-bold">
                  <span class="small">Win Rate</span>
                  <span class="title">99.09%</span>
                </li>                
                <li class="bonus_box semi-bold">
                  <span class="small">Bonus</span>
                  <span class="title">99.09%</span>
                </li>
              </ul>


              <div class="row">

                <div class="col-12 col-md-6">
                  <div class="pros semi-bold">
                    <h3>Pros</h3>
                    <ul>
                        <li>Easy banking methods</li>
                        <li>Compatible with mobile devices</li>
                        <li>International players welcome </li>                 
                    </ul>
                  </div>                  
                </div>                

                <div class="col-12 col-md-6">
                  <div class="review">
                    <span class="small semi-bold">Deposit Methods &amp; Compatibility</span>
                    <ul class="cards_logo d-flex flex-wrap my-2">
                      <li><img src="{{ asset('images/visa-light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/mastercard--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/neteller--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/bitcoin--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/paynearme--light.png') }}" alt="visa"></li>
                    </ul>
                    <div class="star_review_box d-flex align-items-center">
                    <ul class="star_rate d-flex yellow">
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                    <span><strong class="semi-bold">5</strong>/5</span>
                    </div>
                    <a class="read_review semi-bold" href="#">Read Review</a>
                  </div>                  
                </div>





              </div>

            </div>
          </li>             
          
          <li class="gamb_box">
            <div class="left_side">
              <div class="site_logo">
                <div class="img_box"><img src="{{ asset('images/cafe casino.png') }}" alt="logo"></div>
                <a class="visit_site semi-bold" href="#">Visit Site</a>
              </div>
            </div>

            <div class="right_side">
              <ul class="main_bonus_box">
                <li class="bonus_box dark semi-bold">
                  <span class="small">Bonus</span>
                  <span class="yellow title">100% up to $250</span>
                  <span class="condition">Terms &amp; Conditions Apply</span>
                </li>                
                <li class="bonus_box semi-bold">
                  <span class="small">Win Rate</span>
                  <span class="title">99.09%</span>
                </li>                
                <li class="bonus_box semi-bold">
                  <span class="small">Bonus</span>
                  <span class="title">99.09%</span>
                </li>
              </ul>


              <div class="row">

                <div class="col-12 col-md-6">
                  <div class="pros semi-bold">
                    <h3>Pros</h3>
                    <ul>
                        <li>Easy banking methods</li>
                        <li>Compatible with mobile devices</li>
                        <li>International players welcome </li>                 
                    </ul>
                  </div>                  
                </div>                

                <div class="col-12 col-md-6">
                  <div class="review">
                    <span class="small semi-bold">Deposit Methods &amp; Compatibility</span>
                    <ul class="cards_logo d-flex flex-wrap my-2">
                      <li><img src="{{ asset('images/visa-light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/mastercard--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/neteller--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/bitcoin--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/paynearme--light.png') }}" alt="visa"></li>
                    </ul>
                    <div class="star_review_box d-flex align-items-center">
                    <ul class="star_rate d-flex yellow">
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                    <span><strong class="semi-bold">5</strong>/5</span>
                    </div>
                    <a class="read_review semi-bold" href="#">Read Review</a>
                  </div>                  
                </div>





              </div>

            </div>
          </li>

          <li class="gamb_box">
            <div class="left_side">
              <div class="site_logo">
                <div class="img_box"><img src="{{ asset('images/my-bookie.png') }}" alt="logo"></div>
                <a class="visit_site semi-bold" href="#">Visit Site</a>
              </div>
            </div>

            <div class="right_side">
              <ul class="main_bonus_box">
                <li class="bonus_box dark semi-bold">
                  <span class="small">Bonus</span>
                  <span class="yellow title">100% up to $250</span>
                  <span class="condition">Terms &amp; Conditions Apply</span>
                </li>                
                <li class="bonus_box semi-bold">
                  <span class="small">Win Rate</span>
                  <span class="title">99.09%</span>
                </li>                
                <li class="bonus_box semi-bold">
                  <span class="small">Bonus</span>
                  <span class="title">99.09%</span>
                </li>
              </ul>


              <div class="row">

                <div class="col-12 col-md-6">
                  <div class="pros semi-bold">
                    <h3>Pros</h3>
                    <ul>
                        <li>Easy banking methods</li>
                        <li>Compatible with mobile devices</li>
                        <li>International players welcome </li>                 
                    </ul>
                  </div>                  
                </div>                

                <div class="col-12 col-md-6">
                  <div class="review">
                    <span class="small semi-bold">Deposit Methods &amp; Compatibility</span>
                    <ul class="cards_logo d-flex flex-wrap my-2">
                      <li><img src="{{ asset('images/visa-light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/mastercard--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/neteller--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/bitcoin--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/paynearme--light.png') }}" alt="visa"></li>
                    </ul>
                    <div class="star_review_box d-flex align-items-center">
                    <ul class="star_rate d-flex yellow">
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                    <span><strong class="semi-bold">5</strong>/5</span>
                    </div>
                    <a class="read_review semi-bold" href="#">Read Review</a>
                  </div>                  
                </div>





              </div>

            </div>
          </li>           

          <li class="gamb_box">
            <div class="left_side">
              <div class="site_logo">
                <div class="img_box"><img src="{{ asset('images/betonline.png') }}" alt="logo"></div>
                <a class="visit_site semi-bold" href="#">Visit Site</a>
              </div>
            </div>

            <div class="right_side">
              <ul class="main_bonus_box">
                <li class="bonus_box dark semi-bold">
                  <span class="small">Bonus</span>
                  <span class="yellow title">100% up to $250</span>
                  <span class="condition">Terms &amp; Conditions Apply</span>
                </li>                
                <li class="bonus_box semi-bold">
                  <span class="small">Win Rate</span>
                  <span class="title">99.09%</span>
                </li>                
                <li class="bonus_box semi-bold">
                  <span class="small">Bonus</span>
                  <span class="title">99.09%</span>
                </li>
              </ul>


              <div class="row">

                <div class="col-12 col-md-6">
                  <div class="pros semi-bold">
                    <h3>Pros</h3>
                    <ul>
                        <li>Easy banking methods</li>
                        <li>Compatible with mobile devices</li>
                        <li>International players welcome </li>                 
                    </ul>
                  </div>                  
                </div>                

                <div class="col-12 col-md-6">
                  <div class="review">
                    <span class="small semi-bold">Deposit Methods &amp; Compatibility</span>
                    <ul class="cards_logo d-flex flex-wrap my-2">
                      <li><img src="{{ asset('images/visa-light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/mastercard--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/neteller--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/bitcoin--light.png') }}" alt="visa"></li>
                      <li><img src="{{ asset('images/paynearme--light.png') }}" alt="visa"></li>
                    </ul>
                    <div class="star_review_box d-flex align-items-center">
                    <ul class="star_rate d-flex yellow">
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                    <span><strong class="semi-bold">5</strong>/5</span>
                    </div>
                    <a class="read_review semi-bold" href="#">Read Review</a>
                  </div>                  
                </div>

              </div>

            </div>
          </li> 

        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="tandc d-flex flex-wrap align-items-center justify-content-between">
          <p>T&amp;Cs Apply to All Bonuses. 18+ Only. Gamble Responsibly.</p>
          <a class="read_review semi-bold" href="#">See more reviews</a>
        </div>
      </div>
    </div>
  </div>
</div>


<!-------- Game Guides ---------->
<div class="game_guides">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-7">
        <div class="row">
            <div class="col-12">
              <h2 class="hd-2 mb-4">Learn more to win more with our game guides</h2>
              <p>Find out how to play or improve your skills and discover everything you need to wager and win on the most popular online casino games.</p>
              <hr class="my-4">
            </div>

            <div class="col-12">

              <div class="we_rate">
                <small>Check it out!</small>
                <h3>How we rate gambling sites?</h3>
                <p>Our tools and key guides provide you with all the information you need to improve your industry knowledge. Learn how to count cards, how sports betting works and how to win at the casino.</p>
              </div>

              <div class="main_faq">
                <div class="faq_container"> 
                  <div class="faq">
                    <div class="faq_question"> 
                      <span class="question"> <img src="{{ asset('images/dices.png') }}" alt="icon"> Banking Background Check</span>
                      <span class="accordion-button-icon fa fa-angle-down"></span>
                    </div>

                    <div class="faq_answer_container">
                      <div class="faq_answer"><span><p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a, vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem. Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                      </span>
                    </div>
                    </div>
                  </div>
                </div>          

                <div class="faq_container"> 
                  <div class="faq">
                    <div class="faq_question"> 
                      <span class="question"><img src="{{ asset('images/card-game-2.png') }}" alt="icon"> We deposit funds and contact customer support </span>
                      <span class="accordion-button-icon fa fa-angle-down"></span>
                    </div>

                    <div class="faq_answer_container">
                      <div class="faq_answer"><span><p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a, vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem. Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                      </span>
                    </div>
                    </div>
                  </div>
                </div>          

                <div class="faq_container"> 
                  <div class="faq">
                    <div class="faq_question"> 
                      <span class="question"><img src="{{ asset('images/betting2.png') }}" alt="icon"> Play, Win, Cash Out </span>
                      <span class="accordion-button-icon fa fa-angle-down"></span>
                    </div>

                    <div class="faq_answer_container">
                      <div class="faq_answer"><span><p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a, vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem. Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                      </span>
                    </div>
                    </div>
                  </div>
                </div>          

                <div class="faq_container"> 
                  <div class="faq">
                    <div class="faq_question"> 
                      <span class="question"><img src="{{ asset('images/dealer2.png') }}" alt="icon"> Support </span>
                      <span class="accordion-button-icon fa fa-angle-down"></span>
                    </div>

                    <div class="faq_answer_container">
                      <div class="faq_answer"><span><p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a, vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem. Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                      </span>
                    </div>
                    </div>
                  </div>
                </div>

              </div> 
            </div>    
        </div>
      </div>
      <div class="col-12 col-lg-5">
      <div class="icon_slots">
        <ul class="row">
          <li class="col-6">
            <div class="icon_box">
              <img src="{{ asset('images/slot-machine.png') }}" alt="icon">
              <span>Slots</span>
            </div>
          </li>          
          <li class="col-6">
            <div class="icon_box">
              <img src="{{ asset('images/card-game.png') }}" alt="icon">
              <span>Blackjack</span>
            </div>
          </li>          
          <li class="col-6">
            <div class="icon_box">
              <img src="{{ asset('images/casino1.png') }}" alt="icon">
              <span>Roulette</span>
            </div>
          </li>          
          <li class="col-6">
            <div class="icon_box">
              <img src="{{ asset('images/dealer.png') }}" alt="icon">
              <span>Live Dealer</span>
            </div>
          </li>          
          <li class="col-6">
            <div class="icon_box">
              <img src="{{ 'images/casino2.png' }}" alt="icon">
              <span>Poker</span>
            </div>
          </li>          
          <li class="col-6">
            <div class="icon_box">
              <img src="{{ 'images/betting.png' }}" alt="icon">
              <span>Sports Betting</span>
            </div>
          </li>
        </ul>
      </div>
      </div>
    </div>
  </div>
</div><!-------- Blacklisted Casio ----------><div class="blacklist cmn-padd">
  <div class="container">
    <div class="row mb-4 mb-sm-5">
      <div class="col-12">
        <div class="black_list_logo d-flex align-items-center">
          <img src="{{ asset('images/alarm.png') }}" alt="alarm logo">
          <h3>BLACLISTED CASINO</h3>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-4">
        <div class="black_box">
          <img src="{{ asset('images/454555.jpg') }}" alt="logo">
          <div class="grand">Grand Prive <span>2.3/5</span></div>
          <ul class="mb-0">            
            <li>Unclear wagering requirements</li>
            <li>Poor casino practices used</li>
          </ul>
        </div>
      </div>     
      <div class="col-12 col-sm-4">
        <div class="black_box">
          <img src="{{ asset('images/445645.jpg') }}" alt="logo">
          <div class="grand">Planet 7 <span>1.2/5</span></div>
          <ul class="mb-0">            
            <li>Unclear wagering requirements</li>
            <li>Poor casino practices used</li>
          </ul>
        </div>
      </div>     
      <div class="col-12 col-sm-4">
        <div class="black_box">
          <img src="{{ asset('images/45645.jpg') }}" alt="logo">
          <div class="grand">Cool Cat <span>2/5</span></div>
          <ul class="mb-0">            
            <li>Unclear wagering requirements</li>
            <li>Poor casino practices used</li>
          </ul>
        </div>
      </div>
    </div>

    <div class="row">

      <div class="col-12">
        <div class="row casino_program">
          <div class="col-12">
            <hr class="my-4">
          </div>
          <div class="col-12 col-md-4">
            <div class="box">
              <h3>We help you make informed choices online.</h3>
              <p>Our expert reviews, in-depth game guides, blacklisted casinos and latest news will help you make informed decisions as a player.</p>
            </div>
          </div>      

          <div class="col-12 col-md-4">
            <div class="box">
              <h3>Blacklisted casino program to keep players safe.</h3>
              <p>Casinos that operate unethically and under false pretences are put on our blacklist. Use this guide to learn which ones to stay away from.</p>
            </div>
          </div>      

          <div class="col-12 col-md-4">
            <div class="box">
              <h3>Rigorous 25-step review process.</h3>
              <p>Casinos that operate unethically and under false pretences are puOur research is data-driven and follows a detailed 25-step review process. </p>
            </div>
          </div>          
        </div>
      </div>



    </div> 

  </div>
</div><div class="extensive_review cmn-padd">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ul class="mb-0">
          <li>
            <h2 class="hd-2">Extensive Review Process</h2>
            <p>When we started out no sites explained what a parlay was or even what deposit options were available at sportsbooks.</p>
          </li>         
          <li>
            <h2 class="hd-2">Get Verified Bonuses</h2>
            <p>Every sportsbook sign-up bonus and promotion on this site is tested personally so you can be sure there are no issues in claiming what you see. Compare the best offers from the most trusted sportsbooks online in one place.</p>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>


<div class="how_work cmn-padd text-center text-sm-start">
  <div class="container">
    <div class="row">
      <div class="col-12 white position-relative">
            <h2 class="hd-2 mb-4">How Online Gambling Works</h2>
            <p>The online gambling industry in the U.S. is experiencing unprecedented growth, yet continues to be a work in progress. Each state has a different set of online gambling laws, creating something of a jigsaw puzzle for bettors.</p>

            <p>The most aggressive online gambling states such as New Jersey and Pennsylvania offer a full range of online options for casino gambling, poker and sports betting. Two years after the U.S. Supreme Court paved the way, more than a dozen other states have legalized online sports betting, with more on the way. While some states (including New York) only allow in-person sports wagering at physical sportsbooks, it’s clear that online/mobile wagering is the future — in states that allow it, upwards of 80% of all sports wagers are placed online.</p>

            <p>Not sure what your state allows? Gambling.com provides the most up-to-date information on legal and regulated gaming in every state, so follow us for the latest news and updates, as well as the top bonus offers. When you’ve selected a site, we can help you through the sign-up process or suggest the best sites to play casino games for free. In states without legal internet gambling, players can still sign up for social casino or sweepstakes casino, which are completely legal across the United States and sometimes offer real prizes.</p>
      </div>
    </div>
  </div>
</div>


<!-------- Free Games ---------->
<div class="free_games cmn-padd text-center text-sm-start">
  <div class="container">
    <div class="row">
      <div class="col-12 position-relative">
            <h2 class="hd-2 mb-4">Try our free games before you pick a site!</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-6 col-lg-4">
        <div class="card_box">
          <img src="{{ asset('images/05250e1d99.png') }}" alt="image">
          <h3>4 Seasons Online Slot</h3>
          <a class="ylw_play_btn" href="#">Play For Free</a>
        </div>
      </div>      
      <div class="col-12 col-sm-6 col-lg-4">
        <div class="card_box">
          <img src="{{ asset('images/001a34f808.png') }}" alt="image">
          <h3>A Night Out Online Slot</h3>
          <a class="ylw_play_btn" href="#">Play For Free</a>
        </div>
      </div>      
      <div class="col-12 col-sm-6 col-lg-4">
        <div class="card_box">
          <img src="{{ asset('images/e4a313d9aa.png') }}" alt="image">
          <h3>Adventures in Wonderland Online Slot</h3>
          <a class="ylw_play_btn" href="#">Play For Free</a>
        </div>
      </div>        
      <div class="col-12 col-sm-6 col-lg-4">
        <div class="card_box">
          <img src="{{ asset('images/702ca2a2ea.png') }}" alt="image">
          <h3>Age of the Gods: Furious 4 Online Slot</h3>
          <a class="ylw_play_btn" href="#">Play For Free</a>
        </div>
      </div>        
      <div class="col-12 col-sm-6 col-lg-4">
        <div class="card_box">
          <img src="{{ asset('images/838543b905.png') }}" alt="image">
          <h3>Age of the Gods: God of Storms Online Slot</h3>
          <a class="ylw_play_btn" href="#">Play For Free</a>
        </div>
      </div>        
      <div class="col-12 col-sm-6 col-lg-4">
        <div class="card_box">
          <img src="{{ asset('images/5f8c6ae551.png') }}" alt="image">
          <h3>Age of the Gods: Goddess of Wisdom Online Slot</h3>
          <a class="ylw_play_btn" href="#">Play For Free</a>
        </div>
      </div>        
      <div class="col-12 col-sm-6 col-lg-4">
        <div class="card_box">
          <img src="{{ asset('images/75cd6bd47f.png') }}" alt="image">
          <h3>Age of the Gods: King of Olympus Online Slot</h3>
          <a class="ylw_play_btn" href="#">Play For Free</a>
        </div>
      </div>        
      <div class="col-12 col-sm-6 col-lg-4">
        <div class="card_box">
          <img src="{{ asset('images/d18cc76460.png') }}" alt="image">
          <h3>Attraction Online Slot</h3>
          <a class="ylw_play_btn" href="#">Play For Free</a>
        </div>
      </div>        
      <div class="col-12 col-sm-6 col-lg-4">
        <div class="card_box">
          <img src="{{ asset('images/d6b226f0d1.png') }}" alt="image">
          <h3>Bar Bar Black Sheep Online Slot</h3>
          <a class="ylw_play_btn" href="#">Play For Free</a>
        </div>
      </div>      
    </div>
    <div class="row pt-5">
      <div class="col-12">
        <h2 class="hd-2 mb-4">Benefits of Gambling Online</h2>

        <p>Online gambling sites offer superior entertainment to playing in a land-based casino. Perhaps you live in an area miles away from a gambling mecca like Las Vegas or even just your nearest casino, or you live in a country where land-based gambling just isn't available.</p>

        <p>The truth is that in most countries, even a big land-based casino can't compete with what online casinos and gambling sites have to offer. Internet gambling can provide hassle-free sign-ups, super-quick banking, and a choice of games you won't find in a live setting. After all, how many brick 'n mortar casinos near you offer hundreds of slots and table games, and low stakes that cater for casual gamblers?</p>

        <p><span class="semi-bold">A Better Range of Games:</span>
        That's where gambling online really comes into its own. You can choose from a huge selection of games and try many titles out for free first. Many casinos even have sportsbooks, bingo and poker rooms attached. Funds can easily be transferred from one tab to another, giving real-money players in the game even more choice. All players have to do is log into their favorite casino website to be able to play hundreds of slots games, craps, baccarat, video poker, blackjack, roulette, faro, keno and more for real money.</p>

        <p><span class="semi-bold">Big Bonuses:</span>
        Next, online gambling rooms on the web offer lucrative welcome bonuses to new players. These bonuses offer hundreds of dollars, pounds or Euros in free cash, just for playing your favourite games. You may even get some free spins on the latest slot, which is a great way of earning real money without spending a penny.</p>

        <p><span class="semi-bold">Great Security:</span>
        Finally, all online gambling casinos abide by the strictest security standards. All regulated online gambling sites use industry-standard 128-bit or higher encryption to protect players. They will even have independent auditors in place to test the software before it hits the market. Auditors also make sure that a casino site's payouts are accurate on a regular basis.</p>
      </div>
    </div>    
  </div>
</div>




<!-------- Poker Site ---------->
<div class="poker_site">
  <div class="container">
    <div class="row">
      <div class="col-12">
         <h2 class="hd-2 mb-4">Best Online Poker Sites 2020</h2>
         <div class="table-responsive">
        <table class="table table-bordered align-middle text-center">

          <thead>
            <tr>
              <th scope="col">Rank</th>
              <th scope="col">Online Poker Sites  List</th>
              <th scope="col">Welcome Bonus</th>
              <th scope="col">Our Rating</th>
              <th scope="col">Win Rate</th>
              <th scope="col">Play Games &amp; Review</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>GG Poker</td>
              <td>$1,000</td>
              <td>4.6</td>
              <td>99.09%</td>
              <td>
                <a class="red_btn" href="#">Play Now</a>
                <a class="red_btn_light" href="#">Review</a>
              </td>
            </tr>            

            <tr>
              <th scope="row">2</th>
              <td>PokerStars</td>
              <td>$200</td>
              <td>4.4</td>
              <td>98.16%</td>
              <td>
                <a class="red_btn" href="#">Play Now</a>
                <a class="red_btn_light" href="#">Review</a>
              </td>
            </tr>            

            <tr>
              <th scope="row">3</th>
              <td>888 Casino</td>
              <td>$100</td>
              <td>4.3</td>
              <td>97.63%</td>
              <td>
                <a class="red_btn" href="#">Play Now</a>
                <a class="red_btn_light" href="#">Review</a>
              </td>
            </tr>            

            <tr>
              <th scope="row">4</th>
              <td>Partypoker</td>
              <td>$400</td>
              <td>4.2</td>
              <td>96.22%</td>
              <td>
                <a class="red_btn" href="#">Play Now</a>
                <a class="red_btn_light" href="#">Review</a>
              </td>
            </tr>            

            <tr>
              <th scope="row">5</th>
              <td>TigerGaming</td>
              <td>$800</td>
              <td>4.1</td>
              <td>95.88%</td>
              <td>
                <a class="red_btn" href="#">Play Now</a>
                <a class="red_btn_light" href="#">Review</a>
              </td>
            </tr>
           
          </tbody>
        </table>    
        </div>     
      </div>
    </div>
  </div>
</div>










<!-------- Trust Us ---------->
<div class="trust_us cmn-padd">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-lg-6">
        <h2 class="hd-2 mb-4">Why you can trust us?</h2>
        <ul>
          <li>
            <h3>Your safety is our priority</h3>
            <div class="content">
              <p>Your safety is our priority Casino.org has been players’ go to online casino comparison site since 1995. Featuring the latest updates and reviews, our team lives for the online casino community and is determined to provide accurate, transparent, and honest information.</p>
            </div>
          </li>          

          <li>
            <h3>100% Independent and free</h3>
            <div class="content">
              <p>We create content to help players, not casinos. Casino.org is 100% independently owned, which means casinos can’t influence our opinions or research. Our news team is ringfenced and operates completely independently. All our information is free to access and free from influence.</p>
            </div>
          </li>          

          <li>
            <h3>25 Years of experience</h3>
            <div class="content">
              <p>Casino.org have been on a mission to help players win more since 1995. We were the world's first ever online casino site, and have continued to trailblaze in the online gambling world ever since. Today we're trusted as the #1 casino and games reviews provider by over a million players worldwide, each year.</p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-12 col-lg-6">
        <div class="img_wrap text-center text-lg-end">
          <img src="{{ asset('images/photo-1592398191627-25b41eeaa398.png') }}" alt="image">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection