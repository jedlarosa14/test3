<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="ie8"><![endif]-->
<!--[if IE 9 ]><html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<htm lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>@yield('title')</title>
<meta name="description" content="Hello world" />
<meta name="keywords" content= "1,2,3,4" />

<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" media="screen" />
<link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css2?family=open sans:wght@400;600;700&display=swap" rel="stylesheet"> 
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet"> 
<link href="{{ asset('css/slick.css') }}" rel="stylesheet">
<link href="{{ asset('css/slick-theme.css') }}" rel="stylesheet">
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<link href="{{ asset('css/responsive.css') }}" rel="stylesheet">

@stack('styles')
</head>
<body class="gambling">

@include('partials.header')

@yield('content')

@include('partials.footer')

<!-- JavaScript Bundle with Popper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/main-script.js') }}" type="text/javascript"></script>
@stack('scripts')
</body>