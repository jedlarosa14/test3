<!-------- Footer ---------->
<footer>

  <div class="footer-play">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-sm-8 col-md-9 mb-4 mb-sm-0 text-center text-sm-start">
          <h3>Best Online Poker Sites 2020</h3>
          <p>Play at the <strong>best online poker sites</strong> of 2020! With our expert rankings and in-depth reviews of the best online poker sites it's easy to choose the right one. We also provide the best sign-up bonuses for players that open a new account via our PokerListings links.</p>
        </div>
        <div class="col-12 col-sm-4 col-md-3"><a class="ylw_play_btn" href="#"> Play Here</a></div>
      </div>
    </div>
  </div> 

  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="footer-widget">

            <li class="ft-space footer-links">
              <div class="row">
                <div class="col-md-12">
                  <h3>About Us</h3>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <ul>
                    <li><a href="{{ route('contact-us') }}">Contact Us</a></li>
                    <li><a href="{{ route('how-we-rate') }}">How We Rate</a></li>
                    <li><a href="{{ route('get-listed') }}">Get Listed</a></li>
                    <li><a href="{{ route('membership') }}">Membership</a></li>
                    <li><a href="{{ route('careers') }}">Careers</a></li>
                    <li><a href="{{ route('home') }}">USGamblingList.com</a></li>
                  </ul>
                </div>
              </div>
            </li>            

            <li class="ft-space footer-links">
              <div class="row">
                <div class="col-md-12">
                  <h3>Reviews</h3>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <ul>
                    <li><a href="#">22bet</a></li>
                    <li><a href="#">Spin Casino</a></li>
                    <li><a href="#">888 Casino</a></li>
                    <li><a href="#">Casino Midas</a></li>
                    <li><a href="#">Euro Palace</a></li>
                  </ul>
                </div>
              </div>
            </li>

            <li class="ft-space footer-links">
              <div class="row">
                <div class="col-md-12">
                  <h3>Gambling Guides</h3>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <ul>
                    <li><a href="#">Slots</a></li>
                    <li><a href="#">Blackjack</a></li>
                    <li><a href="#">Roulette</a></li>
                    <li><a href="#">Live Dealer</a></li>
                    <li><a href="#">Poker</a></li>
                    <li><a href="#">Sports Betting</a></li>
                  </ul>
                </div>
              </div>
            </li>

            <li class="ft-space">
              <div class="footer-about">
                <h3><a href="#"><img src="{{ asset('images/footer-logo.png') }}" alt="footer-logo"></a></h3>
                <h4>You are in safe hands!</h4>
                <p>Our recommended casinos have been verified. </p>
              </div>
            </li>

          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="stay_connect">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="inner d-flex flex-wrap align-items-center justify-content-center justify-content-md-start">
            <p class="me-2 mb-0">Stay Connected With Us:</p>
            <ul class="d-flex mb-0">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="copyright d-flex flex-wrap align-items-center justify-content-center justify-content-md-between">
            <p class="mb-2 mb-md-0"> Copyright © {{ date("Y") }}, Onlinecasino.com, All Rights Reserved. </p>
            <ul class="footer_menu d-flex flex-wrap mb-0">
            <li><a href="#">Home</a></li> 
            <li><a href="#">Terms of Service</a></li> 
            <li><a href="#">Privacy Policy</a></li> 
            <li><a href="#">Sitemap</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>