<header class="nw_header">
  <nav class="navbar navbar-expand-lg">

    <div class="container">
      <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}" alt="logo"></a>

      <button class="nav-trigger navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="nav-container navbar-collapse collapse" id="navbarCollapse" style="">
        <form>
          <input class="form-control me-2" type="search" placeholder="Search Gambling.com" aria-label="Search">
        </form>        
        <ul class="navbar-nav mb-2 mb-md-0 ms-auto">
          <li class="nav-item active"><a class="nav-link" aria-current="page" href="#"><img src="{{ asset('images/poker-cards.png') }}" alt="card"> Reviews</a></li>
          <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('images/dice.png') }}" alt="dice"> Gambling Guides</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('real-money') }}"><img src="{{ asset('images/chip.png') }}" alt="casino"> Real Money</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('online.casions') }}" tabindex="-1" aria-disabled="true"><img src="{{ asset('images/casino-roulette.png') }}" alt="casino"> Online Casinos</a>
          </li>
        </ul>
      </div>      
    </div>

  </nav>
</header>
