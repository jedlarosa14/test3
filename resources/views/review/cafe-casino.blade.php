@extends('layouts.app')
@section('title') Review - cafe-casino @endsection
@section('content')
@push('styles')
<style>
  :root {
    --bg-main-review: #410744;
    --bg-welcome-box: #410744;
    --bg-full-exp: #410744;
    --color-lightyellow: #FFB300;
    --color-bodyfont: #131313;
    --color-lightborder: #DBDBDB;
    --color-lightbg: #f9f9f9;
    --color-basegreen: #249E56;
    --color-simplegreen: #24824A;
    --color-simplered: #CB2026;
    --color-simplegrey: #e4e4e4;
    --color-cmngrey: #929292;
    --color-whitergba: 255, 255, 255;
    --color-blackrgba: 0, 0, 0;
    --color-brightred: #FF1A00;
  }
</style>
@endpush
<div class="review_page">
  <div class="review_banner text-center text-md-start py-5 white">
    <div class="container">
      <div class="row align-items-center">
        <div
          class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4 mb-lg-0 d-flex justify-content-center justify-content-lg-start">
          <div class="rw_logo text-center py-4 px-3">
            <img src="{{ asset('images/cafe-casino/logo-cafe-casino.png') }}" alt="cafe casino">
          </div>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3  mb-4 mb-lg-0">
          <div class="cash_bonus">
            <p class="d-flex align-items-center justify-content-center justify-content-md-start mb-0"><strong
                class="me-2">500%</strong> <span>Cash Bonus</span></p>
            <p><small>Terms and Conditions apply</small></p>
          </div>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3 d-flex justify-content-center  mb-4 mb-lg-0">
          <ul class="list mb-0">
            <li>Strong parent company</li>
            <li>Accepts US Players</li>
            <li>Ignition Rewards points</li>
            <li>Good selection of games</li>
          </ul>
        </div>

        <div class="col-12 col-sm-6 col-md-12 col-lg-3 text-center">
          <div class="yellow_btn_stars">
            <a class="ylw_play_btn semi-bold" href="#">Visit Site</a>
            <div class="star_review_box d-flex align-items-center justify-content-center mt-3">
              <ul class="star_rate d-flex yellow mb-0">
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
              </ul>
              <span><strong class="semi-bold">4.8</strong>/5</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="promo_nav pt-5 pb-3">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ul class="mb-0 d-flex flex-wrap">
            <li class="bg_special"><a href="#">Cafe Casino <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            </li>
            <li><a href="#">Games</a></li>
            <li><a href="#">Software</a></li>
            <li><a href="#">Mobile<br>Casino</a></li>
            <li><a href="#">Promotions &amp; <br>Bonuses</a></li>
            <li><a href="#">User <br>Reviews</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>


  <div class="welcome_boxes">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ul class="wb_main d-flex flex-wrap mb-0 white">
            <li class="logo_box">
              <div class="rw_logo text-center py-4 px-3">
                <img src="{{ asset('images/cafe-casino/logo-cafe-casino.png') }}" alt="cafe casino">
              </div>
            </li>
            <li>
              <div class="wb_box text-center py-4 px-3">
                <small>Welcome Bonus</small>
                <h3>Up to $5,000</h3>
              </div>
            </li>
            <li>
              <div class="wb_box text-center py-4 px-3">
                <small>Cashout Time</small>
                <h3>1-10 Days</h3>
              </div>
            </li>
            <li class="last">
              <div class="wb_box text-center py-4 px-3">
                <small class="d-flex align-items-center justify-content-center text-center">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>
                  <span class="mx-2">Approved for US Customers</span>
                  <img src="{{ asset('images/united-states.png') }}" alt="us_icon">
                </small>
                <a class="ylw_play_btn semi-bold w-100" href="#">Visit Site</a>
                <div class="star_review_box d-flex align-items-center justify-content-center mt-3">
                  <ul class="star_rate d-flex yellow mb-0">
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                  </ul>
                  <span><strong class="semi-bold">4.8</strong>/5</span>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="cmn_content py-5 text-center text-md-start">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 mb-4 font-20">The Casino Experience at Café Casino</h2>
          <p>Café Casino is a new online casino launched in 2016 aimed at United States players who prefer to use
            Bitcoin to make deposits. The property is associated with some of the following Internet casinos: Bovada,
            Slots.lv, and Ignition Casino. Their brand focuses on slot machines and the casino experience. Casino
            gamblers at Café Casino need not worry about being distracted by poker or sports betting options. They have
            an excellent variety of bonuses and promotions available for new and existing players.</p>
          <p>It doesn’t take long for the observant gambler to realize that Café Casino is focused on traditional casino
            games and gaming. Like many modern online casinos, they use multiple software packages to power these games,
            including Genesis Gaming, Realtime Gaming, and Rival.</p>
        </div>
      </div>
    </div>
  </div>


  <div class="cmn_content bg-color py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md-8 mb-4 mb-md-0 text-center text-md-start">
          <div class="content pe-lg-5">
            <h2 class="hd-2 mb-4">How Popular Is Café Casino?</h2>
            <p>Café Casino is not very popular among US internet users. It is currently ranked 151,144 among sites
              visited in the United States.</p>
          </div>
        </div>
        <div class="col-12 col-md-4 d-flex justify-content-center justify-content-lg-end">
          <a class="ylw_play_btn semi-bold" href="#">Play at Café Casino!</a>
        </div>
      </div>
    </div>
  </div>


  <div class="pros_cons with_methods py-5">
    <div class="container">
      <div class="row">

        <div class="col-12 col-sm-6 col-lg-4 d-flex flex-column">
          <div class="content pointer pros mb-3">
            <h3 class="hd-3">Pros</h3>
            <ul>
              <li>Big welcome bonus up to $5,000</li>
              <li>Mobile-friendly</li>
              <li>Bitcoin accepted</li>
              <li>Generous welcome bonus</li>
              <li>Well designed site with excellent table games</li>
              <li>Fast Bitcoin withdrawals (within 24 hours)</li>
            </ul>
          </div>

          <div class="content pointer cons mb-4 mb-md-0">
            <h3 class="hd-3">Cons</h3>
            <ul>
              <li>Limited deposit options</li>
              <li>No live casino</li>
              <li>No weekly promotions</li>
            </ul>
          </div>
        </div>

        <div class="col-12 col-sm-6 col-lg-4 mb-4 mb-md-0">
          <div class="center_block text-center">
            <div class="logo_bonus">
              <img src="{{ asset('images/cafe-casino/logo-cafe-casino.png') }}" alt="logo">
            </div>
            <div class="inner_content">
              <p class="mb-0">500% <span>Welcome Bonus</span> up to $5,000</p>
              <hr>
              <div class="star_review_box d-flex align-items-center justify-content-center">
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
                <span><strong class="semi-bold">4.8</strong>/5</span>
              </div>
              <hr>
              <p class="mb-0">Deposit Methods</p>
              <ul class="cards_logo d-flex flex-wrap justify-content-center mt-2 mb-3">
                <li><img src="{{ asset('images/visa-light.png') }}" alt="visa"></li>
                <li><img src="{{ asset('images/mastercard--light.png') }}" alt="visa"></li>
                <li><img src="{{ asset('images/bitcoin--light.png') }}" alt="visa"></li>
              </ul>
              <hr>
              <a class="ylw_play_btn semi-bold d-inline-block" href="#">Visit Site</a>
            </div>
          </div>
        </div>

        <div class="col-12 col-lg-4 overall_rate">
          <div class="content">
            <h3 class="hd-3">Ratings</h3>
            <div class="main_star_review_box py-4 py-lg-0">
              <div class="star_review_box mt-3">
                <h4>Overall Rating:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Reliability:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Software &amp; Games:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Payout Speed:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Customer Service:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="full_exp cmn-padd text-center text-sm-start">
    <div class="container">
      <div class="row">
        <div class="col-12 white position-relative">
          <div class="small_box mb-4"><span>#Info | Café Casino, the good stuff.</span></div>
          <h2 class="hd-2 mb-4">Full Experience and Review of the Café Casino Platform</h2>
          <h3>Is Café Casino a Legit Online Casino?</h3>
          <p>Café Casino brings forth a fairly new online platform, first starting out in 2016. And while new casinos
            pop up online all the time, there was something that grabbed me about this one. The title of it is quite
            appealing in itself, giving the sense of relaxation with a cup of coffee. However, the website design
            compliments this completely, bringing intriguing graphics and a wonderful design to the forefront.</p>

          <p>The casino incorporates games from several different developers, including Real Time Gaming, Betsoft and
            Rival Gaming. Therefore, you’ll find quite a well-stacked game lobby is available to access. Within, you can
            play games like 8 Lucky Charms, Atomic Age, Champs Elysees, Dragon Princess and Windy Farm. Alongside these
            slot games, you’ll also find some Table games – including both Blackjack and Roulette – as well as Video
            Poker and a few speciality titles.</p>
        </div>
      </div>
    </div>
  </div>


  <div class="case_review py-5 mb-0 mb-md-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 mb-4">Café Casino Review</h2>
        </div>
        <div class="col-12 col-lg-7">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td>Webiste</td>
                <td>CafeCasino.lv</td>
              </tr>
              <tr>
                <td>Online Since</td>
                <td>2016</td>
              </tr>
              <tr>
                <td>Bonus</td>
                <td>Up to $5,000</td>
              </tr>
              <tr>
                <td>Software</td>
                <td>Rival</td>
              </tr>
              <tr>
                <td>Location</td>
                <td>Costa Rica</td>
              </tr>
              <tr>
                <td>Banking</td>
                <td>MasterCard, Visa, Bitcoin and See Site for More Options</td>
              </tr>
              <tr>
                <td>License</td>
                <td>Kahnawake Gaming Commission</td>
              </tr>
              <tr>
                <td>Players</td>
                <td>US Players Are Accepted</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="col-12 col-lg-5">
          <ul class="progress_bar mb-0">
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Bonus</span>
                <span class="progress_number">8</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Wagering Requirements</span>
                <span class="progress_number">7.8</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 78%" aria-valuenow="78" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Payment Methods</span>
                <span class="progress_number">7.4</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 74%" aria-valuenow="74" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Customer Support</span>
                <span class="progress_number">9.2</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 92%" aria-valuenow="92" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Mobile Casino</span>
                <span class="progress_number">8.5</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 85%" aria-valuenow="85" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Withdrawal Speed</span>
                <span class="progress_number">7.3</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 73%" aria-valuenow="73" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Bonuses &amp; Promotions</span>
                <span class="progress_number">8.5</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 85%" aria-valuenow="85" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>


  <div class="slot_bar py-4 py-xl-5">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 position-relative">
          <div class="content white">
            <img src="{{ asset('images/dice66.png') }}" alt="icon">
            <p>Café Casino offers most traditional casino games, but where the site really shines is in its selection of
              slots games.</p>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="bonus_details cmn_content pt-5 mt-0 mt-md-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 with_tick mb-4">Café Casino Bonus</h2>
          <div class="claim_bonus">
            <div class="claim_bonus_inner">
              <strong><span>500%</span> Cash Bonus up to <span>$5,000</span></strong>
              <span>Terms and conditions apply</span>
            </div>
            <a href="#">Claim</a>
          </div>
          <p>When you sign up to Cafe Casino, make sure you claim one of its two excellent welcome offers. If you like
            to fund your account with cryptocurrencies, our Cafe Casino review experts suggest you opt for the Bitcoin
            bonus that you can use to play an exciting variety of slots and games. </p>

          <p>The second of the Cafe Casino welcome offers is a generous bonus for players that make their first deposit
            using a credit card. As with the Bitcoin bonus, you’ll be able to use both your deposit and bonus funds to
            play any of the games on site.</p>

          <p>There are also lots of existing player promotions going on at Cafe Casino. Make sure you check your inbox
            every Thursday, as this is where you’ll find your weekly mystery bonus. If you like to have fun and win
            prizes, take part in the regular slot tournaments. You can check the full schedule in the ‘Tournaments’ tab.
            There’s also a referral scheme at Cafe Casino, where you can earn more bonuses by getting your friends to
            sign up. </p>

          <ul class="mt-4 mb-0">
            <li>
              <h3>Bonus Details 500% Super Play Welcome Bonus</h3>
              <p>New depositors can take their pick from two different options for the casino’s Super Play Welcome
                Bonus. This is the first option – a match bonus worth up to $5,000 at a match rate of 500%. This bonus
                requires a playthrough of 35x.</p>
            </li>

            <li>
              <h3>100% No-Max Welcome Bonus</h3>
              <p>This is the second welcome bonus option – a match bonus worth up to $1,000 at a match rate of 100%.
                This bonus requires a playthrough of 25x.</p>
            </li>

            <li>
              <h3>$10 Free Chip</h3>
              <p>All new depositors earn $10 in free play in the form of a $10 Free Chip. Any winnings you accrue while
                playing with the $10 in free play is subject to a 60x playthrough requirement, and a max withdrawal
                amount of $125. If you win more than that, you forfeit any amount above $125.</p>
            </li>

            <li>
              <h3>Perks</h3>
              <p>Perks is the name of Café Casino’s rewards program. All players start out with certain rewards by
                virtue of joining the Perks program. Entry-level members enjoy 10% Daily Cashback offers, weekly bonus
                offers, a chance to win a weekly $4,000 prize pool drawing, and the standard spate of bonuses and
                promotions offered above. As you accumulate more rewards points, those numbers start going up. For
                instance, at the next level (in exchange for 50,000 points) you have access to $150 referral fees, more
                entries in the prize pool, and even better bonus offers.</p>
            </li>
          </ul>

        </div>
      </div>
    </div>
  </div>

  <div class="highlight_section pb-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h3 class="hd-3 mb-4">Welcome Bonus</h3>
            <p>Café Casino offers a great loyalty program called Perks. Unlike other sites that have complicated
              programs that make it difficult to get rewarded, Café Casino Perks is renowned for its simplicity. Players
              will get 10% daily cash back, massive weekly bonuses on just about every deposit, and weekly prize draws.
              High rollers stand to get even greater perks including 15% cash back, larger bonuses, and a birthday
              bonus.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="cmn_content pt-0 pb-4 py-md-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h2 class="hd-2 with_tick mb-4">Banking with Café Casino</h2>
            <ul class="cards_logo d-flex flex-wrap mt-2 mb-3">
              <li><img src="{{ asset('images/visa-light.png') }}" alt="visa"></li>
              <li><img src="{{ asset('images/mastercard--light.png') }}" alt="visa"></li>
              <li><img src="{{ asset('images/bitcoin--light.png') }}" alt="visa"></li>
            </ul>
            <p>Opening a new account is easy. You input your name, address, phone number, email address, and some
              security information (mother’s maiden name and city of birth). Once you’ve opened an account, you can move
              on to deposit funds.</p>

            <p>Players in the United States (and indeed, even in other countries), might or might not have trouble
              getting their deposit to go through on any of those credit cards. Gambling transactions are riskier and
              more costly for credit card companies, so they often decline them just as a matter of policy.</p>

            <p>Bitcoin, on the other hand, is safe and secure, and you’ll have no trouble making a deposit using that
              method. In fact, one of the biggest selling points of the casino is their readiness and willingness to
              accept deposits via Bitcoin. It’s a neat way to get money to and from a casino, and it’s not available at
              most online casinos. (It is becoming more popular and accepted, though.)</p>

            <p>You can make deposits in any of the following amounts: $25, $50, $100, $200, $300, $400, $500, $1000.</p>

            <p>What is the payout processing time like here?
              Café Casino boasts of some of the best turnaround times in the business for withdrawals. If you’re using
              Bitcoin, you can get your winnings within 24 hours or less. Otherwise, payouts take between 4 and 10
              working days, which is pretty standard in the online casino industry.</p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="with_methods">
    <div class="container">
      <div class="row">

        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
          <div class="content pointer">
            <h3 class="hd-3">Deposit Methods</h3>
            <ul>
              <li>American Express</li>
              <li>Visa</li>
              <li>Mastercard</li>
              <li>Bitcoin</li>
              <li>Bitcoin Cash</li>
            </ul>
          </div>
        </div>

        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
          <div class="content pointer">
            <h3 class="hd-3">Withdrawal Methods</h3>
            <ul>
              <li>Check</li>
              <li>Bitcoin</li>
              <li>Bitcoin Cash</li>
              <li>American Express</li>
            </ul>
          </div>
        </div>

        <div class="col-12 col-lg-6">
          <div class="content">
            <h3 class="hd-3">Our Review:</h3>
            <p>Café Casino has a lot going for it. We love that they’ve put together an interesting library of games
              that includes a number of variants of classics like blackjack and roulette. And yes, they have a big
              collection of slots to satisfy the hordes of US-based slot fans.</p>
          </div>
        </div>

      </div>
    </div>
  </div>


  <div class="highlight_section py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h3 class="hd-3 mb-4">Banking and Cashouts</h3>
            <p>Making payments at Cafe Casino is easy, and you can use a variety of popular credit cards such as Visa
              and Mastercard. You can also use Bitcoin to fund your casino account. Most of the deposit methods are not
              subject to a fee by Cafe Casino, and the site will never charge you for cryptocurrency withdrawals.</p>
            <p>Our review of Cafe Casino revealed that it’s better to complete your KYC verification checks as soon as
              possible, after which cash withdrawals will be credited within four to 10 days. If you’re a Bitcoin
              player, you’ll be able to withdraw your winnings in just 24 hours. </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="cmn_content py-0 py-md-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h2 class="hd-2 with_tick mb-4">Customer Support</h2>
            <p><strong>Café Casino’s customer support services are currently available in English only.</strong></p>
            <p>The great thing about Café Casino is that you can find help and assistance whenever you need it. There’s
              a great ‘Help’ section that answers some of the most popular questions. However, if you aren’t able to
              find the answers that you need there, you can speak to the customer support team. They are reachable via a
              Live Chat function, by sending an email or by calling them on the telephone.</p>

            <p>As a newly launched site, Café Casino is willing to go above and beyond to earn your trust. The gaming
              site is always ready to answer the call with 24/7 customer service. Just call their toll free number and
              you'll be greeted immediately by friendly and knowledgeable support staff. With a smaller user base than
              industry titans, you can count on personal attention. Whether you want help setting up your account,
              require technical support, or have general questions, you can expect prompt and courteous service.</p>

            <p>If you aren't feeling talkative, feel free to send an email. In our Café Casino online review, we were
              impressed by the speed of the replies. Even in the dead of the night, agents typically reply to our
              messages within a few minutes.</p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="main_faq py-5">
    <div class="container">
      <div class="row">
        <div class="col-12">

          <h2 class="hd-2 with_tick mb-4">Café Casino FAQs</h2>
          <div class="faq_container open">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="true"
                aria-controls="collapseExample">
                <span class="question">Is the mobile version of Café Casino identical to the traditional website?</span>
              </div>

              <div class="faq_answer_container collapse show" id="collapse1">
                <div class="faq_answer"><span>
                    <p>Café Casino didn’t produce a mobile app – instead, they made a version of their traditional
                      online site that translates well to smartphones and tablets. The mobile version of the site is
                      designed for display on the smaller screen of a mobile gadget, and the games are often tailored to
                      that smaller display size as well. The mobile version of the site offers a limited game library,
                      and limited account access.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">What countries are restricted from accessing Café Casino?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse2">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Is Ignition Casino safe to use? </span>
              </div>

              <div class="faq_answer_container collapse" id="collapse3">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Is Café Casino a scam? </span>
              </div>

              <div class="faq_answer_container collapse" id="collapse4">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse5" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">How long do withdrawals from Café Casino stay “pending”? </span>
              </div>

              <div class="faq_answer_container collapse" id="collapse5">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse6" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">How Old Do I Have to Be to Use Café Casino?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse6">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection