@extends('layouts.app')
@section('title') Review - cafe-casino @endsection
@section('content')
@push('styles')
<style>
  :root {
    --bg-main-review: #FF1A00;
    --bg-welcome-box: #FF1A00;
    --bg-full-exp: #131313;
    --bg-infobox: #CB2026;
    --color-lightyellow: #FFB300;
    --color-bodyfont: #131313;
    --color-lightborder: #DBDBDB;
    --color-lightbg: #f9f9f9;
    --color-basegreen: #249E56;
    --color-simplegreen: #24824A;
    --color-simplered: #CB2026;
    --color-simplegrey: #e4e4e4;
    --color-cmngrey: #929292;
    --color-whitergba: 255, 255, 255;
    --color-blackrgba: 0, 0, 0;
    --color-brightred: #FF1A00;
  }
</style>
@endpush
<div class="review_page">

  <div class="review_banner text-center text-md-start py-5 white">
    <div class="container">
      <div class="row align-items-center">
        <div
          class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4 mb-lg-0 d-flex justify-content-center justify-content-lg-start">
          <div class="rw_logo text-center py-4 px-3">
            <img src="{{ asset('images/ignition-casino/logo-ignition-casino.png') }}" alt="Ignition casino">
          </div>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3  mb-4 mb-lg-0">
          <div class="cash_bonus">
            <p class="d-flex align-items-center justify-content-center justify-content-md-start mb-0"><strong
                class="me-2">150%</strong> <span>Cash Bonus</span></p>
            <p><small>Terms and Conditions apply</small></p>
          </div>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3 d-flex justify-content-center  mb-4 mb-lg-0">
          <ul class="list mb-0">
            <li>Strong parent company</li>
            <li>Accepts US Players</li>
            <li>Ignition Rewards points</li>
            <li>Good selection of games</li>
          </ul>
        </div>

        <div class="col-12 col-sm-6 col-md-12 col-lg-3 text-center">
          <div class="yellow_btn_stars">
            <a class="ylw_play_btn semi-bold" href="#">Visit Site</a>
            <div class="star_review_box d-flex align-items-center justify-content-center mt-3">
              <ul class="star_rate d-flex yellow mb-0">
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
              </ul>
              <span><strong class="semi-bold">4.8</strong>/5</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="promo_nav pt-5 pb-3">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ul class="mb-0 d-flex flex-wrap">
            <li class="bg_special"><a href="#">About Ignition Casino <i class="fa fa-long-arrow-right"
                  aria-hidden="true"></i></a></li>
            <li><a href="#">Games</a></li>
            <li><a href="#">Software</a></li>
            <li><a href="#">Mobile<br>Casino</a></li>
            <li><a href="#">Promotions &amp; <br>Bonuses</a></li>
            <li><a href="#">User <br>Reviews</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>


  <div class="welcome_boxes">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ul class="wb_main d-flex flex-wrap mb-0 white">
            <li class="logo_box">
              <div class="rw_logo text-center py-4 px-3">
                <img src="{{ asset('images/ignition-casino/logo-ignition-casino.png') }}" alt="Ignition casino">
              </div>
            </li>
            <li>
              <div class="wb_box text-center py-4 px-3">
                <small>Welcome Bonus</small>
                <h3>Up to $2,000</h3>
              </div>
            </li>
            <li>
              <div class="wb_box text-center py-4 px-3">
                <small>Cashout Time</small>
                <h3>2-4 Days</h3>
              </div>
            </li>
            <li class="last">
              <div class="wb_box text-center py-4 px-3">
                <small class="d-flex align-items-center justify-content-center text-center">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>
                  <span class="mx-2">Approved for US Customers</span>
                  <img src="{{ asset('images/united-states.png') }}" alt="us_icon">
                </small>
                <a class="ylw_play_btn semi-bold w-100" href="#">Visit Site</a>
                <div class="star_review_box d-flex align-items-center justify-content-center mt-3">
                  <ul class="star_rate d-flex yellow mb-0">
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                  </ul>
                  <span><strong class="semi-bold">4.8</strong>/5</span>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="cmn_content py-5 text-center text-md-start">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 mb-4 font-20">A Closer Look Inside of Ignition Casino</h2>
          <p>Ignition Casino is a hot newcomer to the online casino market. While the online casino was was established
            in 2016, it has quickly proven itself as a reliable choice with its license from the Kahnawake Gaming
            Commission. Players looking for a top collection of games can register easily here.</p>
          <p>It’s the same on both desktop and mobile, allowing you to sign up on the go. The welcome bonus will help
            get things started and the special rewards come from every real money bet placed on the casino games.</p>
        </div>
      </div>
    </div>
  </div>





  <div class="pros_cons with_methods py-5">
    <div class="container">
      <div class="row">

        <div class="col-12 col-sm-6 col-lg-4">
          <div class="content pointer pros mb-3">
            <h3 class="hd-3">Pros</h3>
            <ul>
              <li>Big welcome bonus up to $2,000</li>
              <li>Special 200% bonus for Bitcoin users</li>
              <li>Above-average loyalty scheme</li>
              <li>Great selection of Rival Gaming slots</li>
              <li>New live dealer games with image previews</li>
              <li>Fast Bitcoin withdrawals (within 24 hours)</li>
            </ul>
          </div>

          <div class="content pointer cons mb-4 mb-md-0">
            <h3 class="hd-3">Cons</h3>
            <ul>
              <li>Somewhat limited selection of video poker games</li>
              <li>No official license</li>
              <li>No weekly promotions</li>
            </ul>
          </div>
        </div>

        <div class="col-12 col-sm-6 col-lg-4 mb-4 mb-md-0">
          <div class="center_block text-center">
            <div class="logo_bonus">
              <img src="{{ asset('images/ignition-casino/ignition casino.png') }}" alt="logo">
            </div>
            <div class="inner_content">
              <p class="mb-0">150% <span>Welcome Bonus</span> up to $1,000</p>
              <hr>
              <div class="star_review_box d-flex align-items-center justify-content-center">
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
                <span><strong class="semi-bold">4.1</strong>/5</span>
              </div>
              <hr>
              <p class="mb-0">Deposit Methods</p>
              <ul class="cards_logo d-flex flex-wrap justify-content-center mt-2 mb-3">
                <li><img src="{{ asset('images/visa-light.png') }}" alt="visa"></li>
                <li><img src="{{ asset('images/mastercard--light.png') }}" alt="visa"></li>
                <li><img src="{{ asset('images/bitcoin--light.png') }}" alt="visa"></li>
              </ul>
              <hr>
              <a class="ylw_play_btn semi-bold d-inline-block" href="#">Visit Site</a>
            </div>
          </div>
        </div>

        <div class="col-12 col-lg-4 overall_rate">
          <div class="content">
            <h3 class="hd-3">Ratings</h3>
            <div class="main_star_review_box py-4 py-lg-0">
              <div class="star_review_box mt-3">
                <h4>Overall Rating:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Reliability:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Software &amp; Games:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Payout Speed:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Customer Service:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>


  <div class="cmn_content pb-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 with_tick mb-4">Independent Review of Ignition Casino</h2>
          <p>Ignition Casino is a hot newcomer to the online casino market. While the online casino was was established
            in 2016, it has quickly proven itself as a reliable choice with its license from the Kahnawake Gaming
            Commission. Players looking for a top collection of games can register easily here.</p>
          <p>It’s the same on both desktop and mobile, allowing you to sign up on the go. The welcome bonus will help
            get things started and the special rewards come from every real money bet placed on the casino games.</p>
        </div>
      </div>
      <div class="row mt-4">
        <div class="review_cat">
          <ul class="d-flex flex-wrap">
            <li>
              <div class="icon_box d-flex flex-column align-items-center">
                <img src="{{ asset('images/ignition-casino/slot-machine.png') }}" alt="icon">
                <span class="icon_name">slots</span>
              </div>
            </li>
            <li>
              <div class="icon_box d-flex flex-column align-items-center">
                <img src="{{ asset('images/ignition-casino/jackpot.png') }}" alt="icon">
                <span class="icon_name">Video Poker</span>
              </div>
            </li>
            <li>
              <div class="icon_box d-flex flex-column align-items-center">
                <img src="{{ asset('images/ignition-casino/card-game.png') }}" alt="icon">
                <span class="icon_name">Blackjack</span>
              </div>
            </li>
            <li>
              <div class="icon_box d-flex flex-column align-items-center">
                <img src="{{ asset('images/ignition-casino/casino2.png') }}" alt="icon">
                <span class="icon_name">Baccarat</span>
              </div>
            </li>
            <li>
              <div class="icon_box d-flex flex-column align-items-center">
                <img src="{{ asset('images/ignition-casino/betting.png') }}" alt="icon">
                <span class="icon_name">Keno</span>
              </div>
            </li>
            <li>
              <div class="icon_box d-flex flex-column align-items-center">
                <img src="{{ asset('images/ignition-casino/bingo.png') }}" alt="icon">
                <span class="icon_name">Bingo</span>
              </div>
            </li>
            <li>
              <div class="icon_box d-flex flex-column align-items-center">
                <img src="{{ asset('images/ignition-casino/dices.png') }}" alt="icon">
                <span class="icon_name">Craps</span>
              </div>
            </li>
            <li>
              <div class="icon_box d-flex flex-column align-items-center">
                <img src="{{ asset('images/ignition-casino/fortune-wheel.png') }}" alt="icon">
                <span class="icon_name">Roulette</span>
              </div>
            </li>
            <li>
              <div class="icon_box d-flex flex-column align-items-center">
                <img src="{{ asset('images/ignition-casino/poker.png') }}" alt="icon">
                <span class="icon_name">Scratchcards</span>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="cmn_content bg-color py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md-8 mb-4 mb-md-0 text-center text-md-start">
          <div class="content pe-lg-5">
            <h2 class="hd-2 mb-4">Secure banking and professional customer support <br> make sure there aren't any
              issues when playing here.</h2>
            <p>The new technology ensures that no downloads are needed and players will find special collections of
              video slots, alongside big progressives, classic slots and more.</p>
          </div>
        </div>
        <div class="col-12 col-md-4 d-flex flex-wrap justify-content-center justify-content-lg-end">
          <div class="d-flex flex-wrap justify-content-center justify-content-lg-end text-right">
            <img src="{{ asset('images/ignition-casino/logo-ignition-casino.png') }}" alt="logo">
            <a class="ylw_play_btn semi-bold" href="#">Play at Café Casino!</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="full_exp cmn-padd text-center text-sm-start">
    <div class="container">
      <div class="row">
        <div class="col-12 white position-relative">
          <div class="small_box mb-4"><span>#Info | Ignition Casino, the good stuff.</span></div>
          <h2 class="hd-2 mb-4">The Must-Read<br> Ignition Casino Review</h2>
          <h3>Is Ignition Casino a Legit Online Casino?</h3>
          <p>Put your keys in the ignition and warm up your engine, because I’m about to take you on a scenic tour of
            Ignition Casino. We won’t be racing, though, so don’t dart off too fast. It’s better to take your time and
            take in the scenery when reading an online casino review. It gives you a better overall view before you make
            a decision.</p>

          <p>As with my other reviews, I’ll cover just about everything you could possibly want to know here, and I’ll
            keep it real all the way. Once we cross the finish line, I’ll either give you a green light or a big red
            stop sign, and then you can make your own mind up about whether you want to play here or not.</p>
          <p>As with my other reviews, I’ll cover just about everything you could possibly want to know here, and I’ll
            keep it real all the way. Once we cross the finish line, I’ll either give you a green light or a big red
            stop sign, and then you can make your own mind up about whether you want to play here or not.</p>
          <p>So, is Ignition Casino a top-of-the-line casino site or a clapped-out old banger? Let’s put the pedal to
            the metal and find out!</p>
        </div>
      </div>
    </div>
  </div>


  <div class="betting_option pt-5 mt-0 mt-md-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p class="semi-bold">You can choose from a diverse range of games at Ignition Casino:</p>
          <p>With its huge range of games, Ignition Casino offers something for all.</p>
          <ul class="d-flex flex-wrap">
            <li>Slots</li>
            <li>Blackjack Variants</li>
            <li>Craps</li>
            <li>Rummy</li>
            <li>Bingo</li>
            <li>Progressive Jackpot Slots</li>
            <li>Baccarat</li>
            <li>Roulette</li>
            <li>Video Poker Variants</li>
            <li>Scratch cards</li>
          </ul>

          <p>With a single account, you can access the poker platform too. Enjoy poker games at Ignition Casino such as:
          </p>
          <ul class="d-flex flex-wrap">
            <li>Poker</li>
            <li>Pai Gow</li>
            <li>Fixed Limit</li>
            <li>No Limit Hold’em</li>
            <li>Omaha Hi-Lo</li>
            <li>Caribbean Stud</li>
            <li>3-Card Poker</li>
            <li>Pot Limit</li>
            <li>Omaha</li>
          </ul>
          <p>Ignition Casino is a hot newcomer to the online casino market. While the online casino was was established
            in 2016, it has quickly proven itself as a reliable choice with its license from the Kahnawake Gaming
            Commission. Players looking for a top collection of games can register easily here.</p>

          <p>It’s the same on both desktop and mobile, allowing you to sign up on the go. The welcome bonus will help
            get things started and the special rewards come from every real money bet placed on the casino games.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="highlight_section py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h3 class="hd-3 mb-4">Games available</h3>
            <p>Slots, Poker Variants, Progressive Jackpot Slots, Blackjack Variants, Baccarat, Craps, Roulette, Rummy,
              Video Poker Variants, Bingo</p>
          </div>
        </div>
      </div>

      <div class="row mt-5">
        <div class="col-12 col-lg-6">
          <img class="w-100" src="{{ asset('images/ignition-casino/image 1.png') }}" alt="img">
        </div>
        <div class="col-12 col-lg-6">
          <img class="w-100" src="{{ asset('images/ignition-casino/image 2.png') }}" alt="img">
        </div>
      </div>
    </div>
  </div>


  <div class="bonus_details cmn_content">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 with_tick mb-4">Ignition Casino Bonus</h2>
          <div class="claim_bonus">
            <div class="claim_bonus_inner">
              <strong><span>100%</span> Cash Bonus up to <span>$2,000</span></strong>
              <span>Terms and conditions apply</span>
            </div>
            <a href="#">Claim</a>
          </div>
          <p>Similar to other reliable online casinos, Ignition is offering a welcome bonus to all its new customers.
            This welcome bonus is quite substantial and it is exclusive to Casino.org users which will net them an
            incredible $4,500 welcome bonus. This is also a 100% match bonus which means that if you join Ignition
            Casino right now and add $100 to your account, you will receive another $100 from the online casino and thus
            have $200 to play with.</p>

          <p>While the bonus goes up to an impressive sum, it is important to keep in mind that it is subject to a
            betting requirement of 25x. In other words, the entire bonus amount will have to be wagered 25 times before
            being able to withdraw the winnings. This is normal with almost all online casinos and a 25x requirement is
            pretty good. However, not all games contribute in the same way and the gaming options with low volatility
            are generally excluded; such as blackjack, baccarat, roulette and craps.</p>

          <p>Basically slots are the only ones which count 100% so if you are a fan of the spinning reels the bonus will
            prove to be very useful. On the other hand however, if you only plan on playing table games, it will be
            difficult to benefit from the bonus and it will actually make the experience more difficult, so it might be
            better to opt out of it.</p>

          <p>Minimum deposits are of $20 so you have a big range to choose from when deciding how much money to transfer
            on the first deposit to make the most out of the welcome bonus.</p>

          <div class="cmn_content">
            <ul class="arrow_list">
              <li><strong>Tell A Friend:</strong> Another regular promotion at Ignition Casino is the Tell-a-friend
                offer where existing customers can refer friends to join the casino online. As a reward, a bonus of 200%
                of the first deposit made by the friend will be offered to the referring player, up to $100.</li>

              <li><strong>The Weekly Boost:</strong> Reload bonuses are available every week at the casino and they can
                offer up to $700 in free money. A weekly deposit bonus code will be provided for use on transactions in
                order to get 100% up to $100 every time. One code can be used seven times per week.</li>

              <li><strong>Ignition Rewards:</strong> Additional benefits come in the form of Ignition Rewards for real
                money players. This is the loyalty program and is very simple, with Reward Points being granted for
                every bet. Earning more points in any given month will result in a better rank and thus better benefits.
                There are a total of eight levels that range from Steel to Diamond and as you go up the ranking, you
                will unlock reload bonuses, special promos, free chips, bonus draws, birthday gifts and more.</li>

              <li><strong>No Deposit Bonus and Free Spins:</strong> Sometimes you’ll find no deposit bonuses on Ignition
                Casino. At the time of writing, none of these promotions were active. In the past, the casino has
                offered £$5 no deposit bonuses for new players with the bonus code. This used to be REVFREE5 but is
                currently deactivated.</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="highlight_section py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h3 class="hd-3 mb-4">Bonus Requirements</h3>
            <p>The bonus terms are largely simple, straightforward, and easy to follow at Ignition Casino. You probably
              won’t get into trouble for missing out on a complicated (by design) requirement. You’re required to wager
              the full amount (deposit + bonus) 25 times before you can cash out. So, if you claim the full bonus,
              you’ll need to wager a total of $50,000.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="cmn_content pt-0 pb-4 py-md-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h2 class="hd-2 with_tick mb-4">Banking</h2>
            <ul class="cards_logo d-flex flex-wrap mt-2 mb-3">
              <li><img src="{{ asset('images/visa-light.png') }}" alt="visa"></li>
              <li><img src="{{ asset('images/mastercard--light.png') }}" alt="visa"></li>
              <li><img src="{{ asset('images/bitcoin--light.png') }}" alt="visa"></li>
            </ul>
            <p>Any honest review of Bovada will tell you the unvarnished facts about its banking policies. That includes
              what deposit and payout methods are available, whether or not any fees apply, and how long payouts take.
              We’ll do all of that in this section.</p>

            <p>If you’ve ever considered using Bitcoin before but haven’t quite had the incentive, you will when you
              understand Bovada’s fee structure. This casino gives you one free deposit via credit or debit card but
              then charges 5.9% on all Visa and Mastercard transactions. If you opt to use Amex, you’ll pay a 9.9% fee.
            </p>

            <p>With regards to withdrawals, Bitcoin transfers are free, whereas the first check each month is free, and
              a flat fee of $50 is applied to each check thereafter.</p>

            <p>Avoid these unnecessary fees and try using Bitcoin! Trust me when I tell you that it’s nowhere near as
              scary as it seems, and once you try it, you will never go back to everyday payment methods when gambling
              online. Bovada has even created a detailed Bitcoin guide explaining what it is, how to get it, and how to
              use it.</p>

            <p>This is an okay number of ways to make deposits and withdrawals, given that it’s a US casino and the
              number will necessarily be limited as a result. Ideally, I would like to see a few more altcoins accepted,
              although Bovada does allow you to withdraw by Bitcoin Cash.\</p>

            <p>With regards to Bovada’s payout times, expect Bitcoin withdrawals within minutes of them being processed.
              If you choose to withdraw by check, expect your check within 10 days.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="with_methods">
    <div class="container">
      <div class="row">

        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
          <div class="content pointer">
            <h3 class="hd-3">Deposit Methods</h3>
            <ul>
              <li>American Express</li>
              <li>Visa</li>
              <li>Mastercard</li>
              <li>Bitcoin</li>
              <li>Bitcoin Cash</li>
            </ul>
          </div>
        </div>

        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
          <div class="content pointer">
            <h3 class="hd-3">Withdrawal Methods</h3>
            <ul>
              <li>Check</li>
              <li>Bitcoin</li>
              <li>Bitcoin Cash</li>
              <li>Wire Transfer</li>
            </ul>
          </div>
        </div>

        <div class="col-12 col-lg-6">
          <div class="content">
            <h3 class="hd-3">Our Review:</h3>
            <p>We’re not disappointed with the existing lineup of banking methods provided by Ignition Casino. For many
              folks, this set of options will work for their banking needs. However, we do believe the banking methods
              can be improved if Ignition Casino adds in some e-wallet options, including PayPal, Skrill, and Neteller.
            </p>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="highlight_section py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h3 class="hd-3 mb-4">There are also fees attached to any deposit made with credit card.</h3>
            <p>5.9% for Mastercard and Visa and 9.9% for American Express. Bitcoin is the only available withdrawal
              method apart from a check, which has a $100 fee and negates withdrawing for any low rollers.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="bonus_details cmn_content pb-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 with_tick mb-4">Customer Support</h2>
          <p><strong>Getting in touch with the Ignition Casino customer service team can be done via the following
              methods.</strong></p>

          <div class="cmn_content">
            <ul class="arrow_list">
              <li><strong>Email:</strong> There’s a contact form directly on the website which you can use to reach out.
                Click the email icon on the main menu bar, and it will pop up on your screen. Expect replies to take up
                to 24 hours, which means this method is best used for general questions which don’t require fast
                answers.</li>

              <li><strong>Live Chat:</strong> The speech bubble icon is always something I like to see, and it’s the one
                you want to click to use the live chat feature at Ignition Casino. Help is available around the clock,
                and I was impressed with the fast, courteous answers I received when I inquired about bonus terms and
                conditions.</li>

              <li><strong>Telephone:</strong> If you’re not quite at home with digital communication and prefer to pick
                up the phone and chat with someone, call 1-855-370-0600. Obviously, this is a toll-free number for North
                Americans. I don’t advise calling it from anywhere else.</li>

            </ul>
            <p>The customer service here is on point. This is how it should be done. I had read a player complaint about
              it before I reviewed this site, so I wasn’t sure what to expect, but it surpassed my expectations. Bravo,
              Team Ignition!</p>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="main_faq pb-5">
    <div class="container">
      <div class="row">
        <div class="col-12">

          <h2 class="hd-2 with_tick mb-4">Ignition Casino FAQs</h2>
          <div class="faq_container open">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="true"
                aria-controls="collapseExample">
                <span class="question">How does Ignition Casino payout?</span>
              </div>

              <div class="faq_answer_container collapse show" id="collapse1">
                <div class="faq_answer"><span>
                    <p>Withdrawals can be made from Ignition Casino using Bitcoin only. It takes up to 72 hours for the
                      casino to process withdrawals made using Bitcoin or Bitcoin Cash. Once released by the casino,
                      payment should be pretty much instant.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Is Ignition Casino legal in the USA?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse2">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Is Ignition Casino safe to use?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse3">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Where is Ignition Casino located?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse4">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse5" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">How long does it take to withdraw from Ignition Casino? </span>
              </div>

              <div class="faq_answer_container collapse" id="collapse5">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse6" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">How Old Do I Have to Be to Use Ignition Casino?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse6">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection