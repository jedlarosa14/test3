@extends('layouts.app')
@section('title') Review - cafe-casino @endsection
@section('content')
@push('styles')
<style>
  :root {
    --bg-main-review: #CD5127;
    --bg-welcome-box: #CD5127;
    --bg-full-exp: #131313;
    --bg-infobox: #FFB300;
    --color-lightyellow: #FFB300;
    --color-bodyfont: #131313;
    --color-lightborder: #DBDBDB;
    --color-lightbg: #f9f9f9;
    --color-basegreen: #249E56;
    --color-simplegreen: #24824A;
    --color-simplered: #CB2026;
    --color-simplegrey: #e4e4e4;
    --color-cmngrey: #929292;
    --color-whitergba: 255, 255, 255;
    --color-blackrgba: 0, 0, 0;
    --color-brightred: #FF1A00;
  }
</style>
@endpush
<div class="review_page">

  <div class="review_banner black text-center text-md-start py-5 white">
    <div class="container">
      <div class="row align-items-center">
        <div
          class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4 mb-lg-0 d-flex justify-content-center justify-content-lg-start">
          <div class="rw_logo text-center py-4 px-3">
            <img src="{{ asset('images/my-bookie/my-bookie.png') }}" alt="My Bookie">
          </div>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3  mb-4 mb-lg-0">
          <div class="cash_bonus">
            <p class="d-flex align-items-center justify-content-center justify-content-md-start mb-0"><strong
                class="me-2">100%</strong> <span>Cash Bonus</span></p>
            <p><small>Terms and Conditions apply</small></p>
          </div>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3 d-flex justify-content-center  mb-4 mb-lg-0">
          <ul class="list mb-0">
            <li>Strong parent company</li>
            <li>Accepts US Players</li>
            <li>Bitcoin accepted</li>
            <li>Excellent mobile platform</li>
          </ul>
        </div>

        <div class="col-12 col-sm-6 col-md-12 col-lg-3 text-center">
          <div class="yellow_btn_stars">
            <a class="ylw_play_btn semi-bold" href="#">Visit Site</a>
            <div class="star_review_box d-flex align-items-center justify-content-center mt-3">
              <ul class="star_rate d-flex yellow mb-0">
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
              </ul>
              <span><strong class="semi-bold">4.8</strong>/5</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="promo_nav pt-5 pb-3">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ul class="mb-0 d-flex flex-wrap">
            <li class="bg_special"><a href="#">My Bookie <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            </li>
            <li><a href="#">Games</a></li>
            <li><a href="#">Software</a></li>
            <li><a href="#">Mobile<br>Casino</a></li>
            <li><a href="#">Promotions &amp; <br>Bonuses</a></li>
            <li><a href="#">User <br>Reviews</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>


  <div class="welcome_boxes black">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ul class="wb_main d-flex flex-wrap mb-0 white">
            <li class="logo_box">
              <div class="rw_logo text-center py-4 px-3">
                <img src="{{ asset('images/my-bookie/my-bookie.png') }}" alt="My Bookie">
              </div>
            </li>
            <li>
              <div class="wb_box text-center py-4 px-3">
                <small>Welcome Bonus</small>
                <h3>Up to $1,000</h3>
              </div>
            </li>
            <li>
              <div class="wb_box text-center py-4 px-3">
                <small>Cashout Time</small>
                <h3>1-2 Days</h3>
              </div>
            </li>
            <li class="last">
              <div class="wb_box text-center py-4 px-3">
                <small class="d-flex align-items-center justify-content-center text-center">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>
                  <span class="mx-2">Approved for US Customers</span>
                  <img src="{{ asset('images/united-states.png') }}" alt="us_icon">
                </small>
                <a class="ylw_play_btn semi-bold w-100" href="#">Visit Site</a>
                <div class="star_review_box d-flex align-items-center justify-content-center mt-3">
                  <ul class="star_rate d-flex yellow mb-0">
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                  </ul>
                  <span><strong class="semi-bold">4.8</strong>/5</span>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="cmn_content py-5 text-center text-md-start">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 mb-4 font-20">MyBookie.ag Sportsbook & Casino Review</h2>
          <p>You have no shortage of options if you’re looking to bet on sports or play real money casino games online.
            Online betting sites are plentiful these days, but a certain few sites stand out above the rest. One of the
            very best all-around sites you will find is MyBookie, which checks just about every box when it comes to
            what you want in an internet-based betting experience.</p>
          <p>Perhaps the first thing we noticed when reviewing MyBookie is the bright, easily-navigable layout. In
            addition to boasting one of the top online sportsbooks for American players, MyBookie also has an array of
            casino games and some of the most advantageous bonuses in the industry. These are just a few of the reasons
            that MyBookie is our top-rated online betting site for 2020.</p>
          <p>Continue reading our detailed MyBookie gambling site review for all the details on what makes this site
            worth your business.</p>
        </div>
      </div>
    </div>
  </div>


  <div class="cmn_content bg-color py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md-8 mb-4 mb-md-0 text-center text-md-start">
          <div class="content pe-lg-5">
            <h2 class="hd-2 mb-4">10+ Sports Betting & Casino Promotions</h2>
            <p>MyBookie.ag offers quite a few promotions. We’ll cover their core offers in the first section below.
              After that we’ll give you a shortlist of their offers for existing customers.</p>
          </div>
        </div>
        <div class="col-12 col-md-4 d-flex justify-content-center justify-content-lg-end">
          <a class="ylw_play_btn semi-bold" href="#">Play at MyBookie.ag</a>
        </div>
      </div>
    </div>
  </div>


  <div class="pros_cons with_methods py-5">
    <div class="container">
      <div class="row">

        <div class="col-12 col-sm-6 col-lg-4 d-flex flex-column">
          <div class="content pointer pros mb-3">
            <h3 class="hd-3">Pros</h3>
            <ul>
              <li>Uses Reputable Sportsbook Software</li>
              <li>Live Betting Available</li>
              <li>Large Sign-up Bonus</li>
              <li>Bitcoin accepted</li>
              <li>Large Welcome Bonus</li>
              <li>Excellent mobile platform</li>
            </ul>
          </div>

          <div class="content pointer cons mb-4 mb-md-0">
            <h3 class="hd-3">Cons</h3>
            <ul>
              <li>Fairly New Brand</li>
              <li>No eWallet Cashier Option</li>
              <li>International Sports Selection is Small</li>
            </ul>
          </div>
        </div>

        <div class="col-12 col-sm-6 col-lg-4 mb-4 mb-md-0">
          <div class="center_block text-center">
            <div class="logo_bonus">
              <img src="{{ asset('images/my-bookie/my-bookie.png') }}" alt="logo">
            </div>
            <div class="inner_content">
              <p class="mb-0">500% <span>Welcome Bonus</span> up to $5,000</p>
              <hr>
              <div class="star_review_box d-flex align-items-center justify-content-center">
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
                <span><strong class="semi-bold">4.8</strong>/5</span>
              </div>
              <hr>
              <p class="mb-0">Deposit Methods</p>
              <ul class="cards_logo d-flex flex-wrap justify-content-center mt-2 mb-3">
                <li><img src="{{ asset('images/visa-light.png') }}" alt="visa"></li>
                <li><img src="{{ asset('images/mastercard--light.png') }}" alt="visa"></li>
                <li><img src="{{ asset('images/bitcoin--light.png') }}" alt="visa"></li>
              </ul>
              <hr>
              <a class="ylw_play_btn semi-bold d-inline-block" href="#">Visit Site</a>
            </div>
          </div>
        </div>

        <div class="col-12 col-lg-4 overall_rate">
          <div class="content">
            <h3 class="hd-3">Ratings</h3>
            <div class="main_star_review_box py-4 py-lg-0">
              <div class="star_review_box mt-3">
                <h4>Overall Rating:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Reliability:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Software &amp; Games:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Payout Speed:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>

              <div class="star_review_box mt-3">
                <h4>Customer Service:</h4>
                <ul class="star_rate d-flex yellow mb-0">
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="full_exp cmn-padd text-center text-sm-start">
    <div class="container">
      <div class="row">
        <div class="col-12 white position-relative">
          <div class="small_box mb-4"><span>#Info | MyBookie.ag, the good stuff.</span></div>
          <h2 class="hd-2 mb-4">MyBookie.ag offers TONS of slots, table, video poker and live dealer games, all of which
            come from BetSoft.</h2>
          <h3>Is Café Casino a Legit Online Casino?</h3>
          <p>Working with BetSoft has its pros and cons.</p>

          <p>On the upside, BetSoft offers a great selection of unique games you won’t find elsewhere. You’ll know what
            I mean when you play their 3D slots. They have tons of recognizable titles that look great and are loads of
            fun to play.</p>

          <p>The downside is that BetSoft was part of a scandal not long ago where they failed to pay out a progressive
            jackpot winner when the casino decided not to pay them.</p>

          <p>The bottom line is that it’s hard to trust a casino who works with a software company who appears to be
            missing their moral compass.</p>

          <p>Ultimately you’ll have to decide if that’s going to scare you away or not. We’ve certainly lost some
            respect for them, even after years of touting their games as fun and unique.</p>

          <p>Anyway, here’s a list of their games and any thoughts we may have about them.</p>
        </div>
      </div>
    </div>
  </div>


  <div class="case_review py-5 mb-0 mb-md-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 mb-4">Café Casino Review</h2>
        </div>
        <div class="col-12 col-lg-7">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td>Webiste</td>
                <td>https://mybookie.ag/</td>
              </tr>
              <tr>
                <td>Online Since</td>
                <td>2014</td>
              </tr>
              <tr>
                <td>Bonus</td>
                <td>Up to $1,000</td>
              </tr>
              <tr>
                <td>Owner</td>
                <td>Private Ownership</td>
              </tr>
              <tr>
                <td>Location</td>
                <td>San Jose, Costa Rica</td>
              </tr>
              <tr>
                <td>Banking</td>
                <td>Bitcoin, ECheck, Mastercard, Person 2 Person, Visa</td>
              </tr>
              <tr>
                <td>License</td>
                <td>Curacao</td>
              </tr>
              <tr>
                <td>Players</td>
                <td>US Players Are Accepted</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="col-12 col-lg-5">
          <ul class="progress_bar mb-0">
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Bonus</span>
                <span class="progress_number">8</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Wagering Requirements</span>
                <span class="progress_number">7.8</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 78%" aria-valuenow="78" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Payment Methods</span>
                <span class="progress_number">7.4</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 74%" aria-valuenow="74" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Customer Support</span>
                <span class="progress_number">9.2</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 92%" aria-valuenow="92" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Mobile Casino</span>
                <span class="progress_number">8.5</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 85%" aria-valuenow="85" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Withdrawal Speed</span>
                <span class="progress_number">7.3</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 73%" aria-valuenow="73" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
            <li>
              <div class="progress_wrap">
                <span class="progress_name">Bonuses &amp; Promotions</span>
                <span class="progress_number">8.5</span>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 85%" aria-valuenow="85" aria-valuemin="0"
                  aria-valuemax="100"></div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>


  <div class="slot_bar py-4 py-xl-5">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 position-relative">
          <div class="content white">
            <img src="{{ asset('images/dice66.png') }}" alt="icon">
            <p>New Player Offers 100% <br>Sports Sign Up Bonus</p>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="bonus_details cmn_content pt-5 mt-0 mt-md-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="hd-2 with_tick mb-4">MyBookie Bonus</h2>
          <div class="claim_bonus">
            <div class="claim_bonus_inner">
              <strong><span>500%</span> Cash Bonus up to <span>$1,000</span></strong>
              <span>Terms and conditions apply</span>
            </div>
            <a href="#">Claim</a>
          </div>
          <p>This is good on your first deposit of at least $100. The max you can deposit is $1,000 (for the bonus).
            Smaller deposits are still eligible for a bonus, but the percentage is smaller. </p>

          <p>Rollover requirements for this bonus must be met in the sportsbook - play in the casino doesn't contribute.
          </p>

          <p>Their terms also state that you cannot make a cash out within 30 days of accepting this promotion. And free
            plays are only good on moneylines where the line is between -200 and +200. </p>

          <ul class="mt-4 mb-0">
            <li>
              <h3>20% Cash Bonus</h3>
              <p>This is good on your (qualifying) first deposit. The maximum is $500 and it has a 10-50x rollover.</p>
            </li>

            <li>
              <h3>100% Casino Bonus</h3>
              <p>This is for first time casino accounts. This is good up to $300. It has a 50x playthrough on both the
                deposit and bonus. Blackjack, craps, video poker, baccarat, sic bo, pai gow and roulette are all
                ineligible for meeting rollover requirements. This sucks, but looking at their 50x rollover for slots
                players, I’m not so sure you’d want a (table game) bonus anyway.</p>
            </li>

            <li>
              <h3>Existing Customer Offers 25% Reload Sports Bonus</h3>
              <p>This is good up to $500. The playthrough is 5-15x depending on how much you deposit. Otherwise, it has
                the same terms as the sports deposit bonus above.</p>
            </li>

            <li>
              <h3>7% Rebate</h3>
              <p>This is good in the race book. You’ll receive a 7% rebate daily and there’s no rollover requirements.
              </p>

              <p>However, the rebate is more or less meant as a bonus, and cannot be cashed out immediately after you
                receive it. My Bookie doesn’t say how long you have to wait, but I’d assume they’re like the offers
                above where you have to wait 30 days.</p>
            </li>

            <li>
              <h3>Refer-a-Friend</h3>
              <p>Send friends to MyBookie.ag and receive a 100% match up to $100 for each referral. If you are playing
                on sports, this bonus will have a five (5) time rollover and if you decide to play in the Casino the
                rollover required will be 30 times (with the same game restrictions mentioned above).</p>
            </li>

            <li>
              <h3>MyBookie.Ag Online Betting Bonuses</h3>
              <p>MyBookie offers a number of freeplay bonuses when players sign up and re-deposit. They have a 10x
                rollover which is better than the industry standard of 15x. They also have many promotional bonuses each
                year for specific events, such as the start of NFL season, NBA Finals, and more.</p>

              <p>These reload bonuses will have rollovers from 5x-10x. Their main deposit bonus offer is a 100% Bonus up
                to $1,000 in freeplay and it comes with a 10x rollover.</p>
              <p>This bonus is available for all players only on their first deposit. All bonuses take 30 days to clear
                and as mentioned above, players cannot cash out until they clear the bonus.</p>
            </li>
          </ul>

        </div>
      </div>
    </div>
  </div>

  <div class="highlight_section pb-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h3 class="hd-3 mb-4">Thoughts on Their Offers</h3>
            <p>They have A LOT of offers, which is great to see. There’s a little bit of everything, too, between the
              deposit bonuses, cash back, rebates, free bets and finder’s fees. However, I don’t like how they have a
              range on their rollover requirements, where the more you deposit, the more you’ll have to roll over.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="cmn_content pt-0 pb-4 py-md-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h2 class="hd-2 with_tick mb-4">Banking with MyBookie.ag | Real Money Banking Options</h2>
            <ul class="cards_logo d-flex flex-wrap mt-2 mb-3">
              <li><img src="{{ asset('images/visa-light.png') }}" alt="visa"></li>
              <li><img src="{{ asset('images/mastercard--light.png') }}" alt="visa"></li>
              <li><img src="{{ asset('images/bitcoin--light.png') }}" alt="visa"></li>
            </ul>
            <p>It’s good to have options. Not every bank or credit card company will allow customers to make financial
              transactions with known betting operators. That’s why it’s important that MyBookie accepts a variety of
              different payment methods that you can use to make your deposits and withdrawals on the site. You have
              plenty of banking options at your disposal, so you should be able to find one that suits your needs.</p>

            <p>Cryptocurrency has become hugely popular in recent years, and MyBookie is one of the few sites that now
              accepts Bitcoin as a preferred method of payment. </p>

            <p>Withdrawal Methods</p>
            <p>MyBookie prides itself on offering fast withdrawals so that you don’t have to wait around for your
              winnings to get from your betting account to your bank account. Requesting a withdrawal on the site or via
              the mobile app is incredibly easy. </p>

            <p>To request a withdrawal, find the “Payout” option in the cashier tab on the site or in the mobile app.
              You will be prompted to verify your phone number, and the site will then send you a verification code to
              make sure you’re the one requesting the withdrawal. Once the site has confirmed that it’s you, the
              withdrawal process will get underway. </p>

            <p>The actual withdrawal can take between a few hours and a few days to be completed. Any payouts requested
              after 2:00pm ET on a business day will be reviewed the next day, and payouts are sent out during normal
              banking hours.</p>
            <p>You may request a withdrawal via a wire transfer, an eCheck, or via Bitcoin. MyBookie having a variety of
              different withdrawal methods is yet another thing that separates them from their competitors.</p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="with_methods">
    <div class="container">
      <div class="row">

        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
          <div class="content pointer">
            <h3 class="hd-3">Deposit Methods</h3>
            <ul>
              <li>American Express</li>
              <li>Visa</li>
              <li>Mastercard</li>
              <li>Bitcoin</li>
              <li>Bitcoin Cash</li>
              <li>Litecoin</li>
              <li>Person2Person</li>
            </ul>
          </div>
        </div>

        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
          <div class="content pointer">
            <h3 class="hd-3">Withdrawal Methods</h3>
            <ul>
              <li>eCheck</li>
              <li>Bitcoin</li>
              <li>Person to Person</li>
            </ul>
          </div>
        </div>

        <div class="col-12 col-lg-6">
          <div class="content">
            <h3 class="hd-3">Our Review:</h3>
            <p>Though MyBookie.ag seems to offer only a handful of payout options, they are all quick and reliable. The
              downside to some of the FIAT money cashouts are the associated fees and perhaps the processing time.
              However, you can always expect from MyBookie Sports to honor a request for withdrawal without any hassle.
            </p>
          </div>
        </div>

      </div>
    </div>
  </div>


  <div class="dotted_boxes pt-5 text-center">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-4">
          <div class="dot_boxes">
            <h3>25% UP TO $500 <br>RELOAD BONUS</h3>
            <p>MyBookie has a reload bonus for all existing users if they want to refund their accounts. All players can
              qualify for a 25% Reload Up to $500. The rollover is less for this bonus, at just 5x, compared to 10x for
              the Sign-Up Bonus.</p>
          </div>
        </div>
        <div class="col-12 col-md-4">
          <div class="dot_boxes">
            <h3>RISK-FREE <br>$20 BET</h3>
            <p>When you open a new account you first $25 bet will earn you a bonus if it loses. If your first real-money
              wager loses, you can email promo@mybookie.cr and receive up to a $20 freeplay. It comes with a 5x
              rollover.</p>
          </div>
        </div>
        <div class="col-12 col-md-4">
          <div class="dot_boxes">
            <h3>25% SPORTS <br>RELOAD UP TO $500</h3>
            <p>MyBookie has a reload bonus for all existing users if they want to refund their accounts. All players can
              qualify for a 25% Reload Up to $500. The rollover is less for this bonus, at just 5x, compared to 10x for
              the Sign-Up Bonus.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="highlight_section py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h3 class="hd-3 mb-4">Banking and Cashouts</h3>
            <p>Making payments at Cafe Casino is easy, and you can use a variety of popular credit cards such as Visa
              and Mastercard. You can also use Bitcoin to fund your casino account. Most of the deposit methods are not
              subject to a fee by Cafe Casino, and the site will never charge you for cryptocurrency withdrawals.</p>
            <p>Our review of Cafe Casino revealed that it’s better to complete your KYC verification checks as soon as
              possible, after which cash withdrawals will be credited within four to 10 days. If you’re a Bitcoin
              player, you’ll be able to withdraw your winnings in just 24 hours. </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="cmn_content py-0 py-md-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="content">
            <h2 class="hd-2 with_tick mb-4">Customer Support</h2>
            <p><strong>Lots of people underrate the importance of finding an online betting site that takes customer
                service seriously. Don’t be one of those people.</strong></p>
            <p>You can get in touch using one of several trusted methods. MyBookie will allow you to connect using
              popular methods such as live chat assistance, phone call or simply emailing via the contact page.</p>

            <p>Getting in touch is easy. You can simply place a call on the US toll-free land-line 844-866-BETS (2387)
              or jump in a live chat with customer support. You will hardly need to wait long for a live agent to come
              to your rescue and many people actually prefer this way.</p>

            <p>If you are in no particular hurry, you may find the last method the handiest, as it would allow you to
              just send an email through the contact form in the Contact Us section of the sportsbook.</p>

            <p>Part of our review process involves independently investigating a site’s customer service quality. Not
              every site is so user-friendly, Remember when we said it’s good to have options? Well, with MyBookie’s
              customer support, you do.</p>

            <p>MyBookie’s Help Center can be found in the upper right-hand corner of their site. From here, you will
              find a massive menu featuring a number of help portals across a few different parts of the site, including
              payment methods, bonuses, rollovers, loyalty programs, the sportsbook, and the casino.</p>

            <p>Consulting this page may help you answer whatever question you may have. If not, there’s a big, green
              “Contact Support” button at the very bottom of the page. If you click that, you’ll be taken to the contact
              page that includes three different methods to get in touch with MyBookie’s support staff. </p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="main_faq py-5">
    <div class="container">
      <div class="row">
        <div class="col-12">

          <h2 class="hd-2 with_tick mb-4">MyBookie FAQs</h2>
          <div class="faq_container open">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="true"
                aria-controls="collapseExample">
                <span class="question">Is This MyBookie Review Biased?</span>
              </div>

              <div class="faq_answer_container collapse show" id="collapse1">
                <div class="faq_answer"><span>
                    <p>Questions like this are important for you to ask anytime you're reviewing or reading a review of
                      an online sportsbook. Why? Because a lot of the MyBookie reviews out there are heavily biased. Too
                      many people put profits before people. But that's not something that happens here. Our review of
                      MyBookie.ag is 100% unbiased. The entire goal of the review is to give you an honest and accurate
                      look at the site so that you can decide if it's a good fit for you or not.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Who Conducts the Review of MyBookie?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse2">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Is MyBookie a Safe Betting Site to Use in 2020? </span>
              </div>

              <div class="faq_answer_container collapse" id="collapse3">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Besides Sports, Can You Play Casino Games at MyBookie.ag? </span>
              </div>

              <div class="faq_answer_container collapse" id="collapse4">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse5" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">Can I Play Slot Games at MyBookie Casino? </span>
              </div>

              <div class="faq_answer_container collapse" id="collapse5">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="faq_container">
            <div class="faq">
              <div class="faq_question" data-bs-toggle="collapse" data-bs-target="#collapse6" aria-expanded="false"
                aria-controls="collapseExample">
                <span class="question">How Old Do I Have to Be to Use MyBookie Casino?</span>
              </div>

              <div class="faq_answer_container collapse" id="collapse6">
                <div class="faq_answer"><span>
                    <p>Phasellus pretium blandit purus ut cursus. Vivamus tempus, nisi quis tincidunt aliquam, elit
                      lectus rutrum ex, ac venenatis diam lorem non justo. Vivamus vel nulla maximus, laoreet est a,
                      vulputate dui. Ut lectus est, ultrices sed lacinia non, egestas et erat. Orci varius natoque
                      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque et placerat lorem.
                      Pellentesque congue ligula quis ipsum auctor rutrum. Etiam eros risus, iaculis fin.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


</div>
@endsection