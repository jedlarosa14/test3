@extends('layouts.app')
@section('title') Home page @endsection
@section('content')
<div class="sitemap">
  <div class="container">
    <div class="row">
      <div class="col-12 hf_bar">
        <h2>USGamblingList.com Sitemap</h2>
      </div>
      <div class="col-12 col-sm-4">
        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Top Guides</h3>
          <ul class="mb-0">
            <li><a href="#">Ultimate App Guide</a></li>
            <li><a href="#">Android</a></li>
            <li><a href="#">Blacklisted Casinos</a></li>
            <li><a href="#">Progressive Jackpots</a></li>
            <li><a href="#">Bonuses</a></li>
            <li><a href="#">No Deposit</a></li>
            <li><a href="#">Free Spins</a></li>
            <li><a href="#">Free Bets</a></li>
            <li><a href="#">Cheating Guide</a></li>
            <li><a href="#">Casino Dealer Guide</a></li>
            <li><a href="#">Download</a></li>
            <li><a href="#">Free Casino Games</a></li>
            <li><a href="#">Gambling Addiction</a></li>
            <li><a href="#">Guide to Online Casinos</a></li>
            <li><a href="#">iPad</a></li>
            <li><a href="#">iPhones</a></li>
            <li><a href="#">Mobile</a></li>
            <li><a href="#">New Casinos</a></li>
            <li><a href="#">Payouts</a></li>
            <li><a href="#">Online Pokies Australia</a></li>
            <li><a href="#">Online Pokies NZ</a></li>
            <li><a href="#">Real Money</a></li>
            <li><a href="#">Scratch Cards</a></li>
            <li><a href="#">Security Guide</a></li>
            <li><a href="#">Social</a></li>
            <li><a href="#">Smartwatches</a></li>
            <li><a href="#">Tablets</a></li>
            <li><a href="#">Top Ten Casino Tips</a></li>
            <li><a href="#">VIP</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Payment Method</h3>
          <ul class="mb-0">
            <li><a href="#">Casino Deposit Methods</a></li>
            <li><a href="#">American Express</a></li>
            <li><a href="#">Bitcoin</a></li>
            <li><a href="#">Credit Card</a></li>
            <li><a href="#">Debit Card</a></li>
            <li><a href="#">eCheck</a></li>
            <li><a href="#">Eco Card</a></li>
            <li><a href="#">ecoPayz</a></li>
            <li><a href="#">Fastest Payout</a></li>
            <li><a href="#">iDeal</a></li>
            <li><a href="#">InstaDebit</a></li>
            <li><a href="#">Interac</a></li>
            <li><a href="#">PayNearMe</a></li>
            <li><a href="#">Pay By Phone</a></li>
            <li><a href="#">PayPal</a></li>
            <li><a href="#">PaySafeCard</a></li>
            <li><a href="#">MasterCard</a></li>
            <li><a href="#">MyCitadel</a></li>
            <li><a href="#">Neteller</a></li>
            <li><a href="#">Trustly</a></li>
            <li><a href="#">Visa</a></li>
            <li><a href="#">Wire Transfer</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Tools & Guides</h3>
          <ul class="mb-0">
            <li><a href="#">Tools & Guides</a></li>
            <li><a href="#">Blackjack Card Counting</a></li>
            <li><a href="#">The Truth about Playing Slots</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Tools & Guides</h3>
          <ul class="mb-0">
            <li><a href="#">Online Gambling</a></li>
            <li><a href="#">Casino & Gambling Glossary</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Features Center</h3>
          <ul class="mb-0">
            <li><a href="#">Features Center</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Features Center</h3>
          <ul class="mb-0">
            <li><a href="#">Casino Employment Opportunities</a></li>
            <li><a href="#">Gambling ages around the world</a></li>
            <li><a href="#">Guide to Rigged Casinos</a></li>
            <li><a href="#">Top 10 Tips for Casino Players</a></li>
            <li><a href="#">NFL Betting Calculator</a></li>
            <li><a href="#">NFL Draft Value</a></li>
            <li><a href="#">NFL Weather Statistics</a></li>
            <li><a href="#">Sports Betting Odds</a></li>
            <li><a href="#">Salary vs. Jackpot</a></li>
            <li><a href="#">What are the Odds?</a></li>
            <li><a href="#">Worldwide Gambling Statistics</a></li>
            <li><a href="#">House Edge</a></li>
            <li><a href="#">Crime in Top Casinos</a></li>
            <li><a href="#">Posting on Instagram in Vegas</a></li>
            <li><a href="#">The Winning Sports of Instagram</a></li>
            <li><a href="#">Superstitious States</a></li>
            <li><a href="#">Richest NBA Players</a></li>
            <li><a href="#">Richest NFL Players</a></li>
            <li><a href="#">Odds of Regretting a Tattoo</a></li>
            <li><a href="#">Biggest Sports Fines</a></li>
            <li><a href="#">Jeopardy Game Show Odds</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Features Center</h3>
          <ul class="mb-0">
            <li><a href="#">Australia - Sports Betting</a></li>
            <li><a href="#">Canada - Sports Betting</a></li>
            <li><a href="#">India - Online Betting</a></li>
            <li><a href="#">New Zealand - Sports Betting</a></li>
            <li><a href="#">UK - Online Betting</a></li>
            <li><a href="#">Esports Betting</a></li>
            <li><a href="#">NFL Betting</a></li>
            <li><a href="#">Super Bowl Betting</a></li>
            <li><a href="#">Cricket Betting</a></li>
            <li><a href="#">T20 World Cup Betting</a></li>
            <li><a href="#">Basketball Betting</a></li>
            <li><a href="#">NBA Betting</a></li>
            <li><a href="#">NHL Betting</a></li>
            <li><a href="#">Football Betting</a></li>
          </ul>
        </div>
      </div>

      <div class="col-12 col-sm-4">

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Casino Games</h3>
          <ul class="mb-0">
            <li><a href="#">Casino Games</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Slot Games</h3>
          <ul class="mb-0">
            <li><a href="#">Slot Features</a></li>
            <li><a href="#">Free Slots Games</a></li>
            <li><a href="#">How to Win at Slots</a></li>
            <li><a href="#">How to Play Slots</a></li>
            <li><a href="#">Mobile Slots</a></li>
            <li><a href="#">Slots Guide</a></li>
            <li><a href="#">Slot Themes</a></li>
            <li><a href="#">Slot Tips</a></li>
            <li><a href="#">Slot Reviews</a></li>
            <li><a href="#">Video Slots</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Blackjack Games</h3>
          <ul class="mb-0">
            <li><a href="#">Blackjack Guide</a></li>
            <li><a href="#">Blackjack Atlantic City</a></li>
            <li><a href="#">Blackjack Switch</a></li>
            <li><a href="#">Blackjack Surrender</a></li>
            <li><a href="#">European Blackjack</a></li>
            <li><a href="#">Blackjack Perfect</a></li>
            <li><a href="#">Free Blackjack Games</a></li>
            <li><a href="#">Blackjack Tips</a></li>
            <li><a href="#">Blackjack Mobile</a></li>
            <li><a href="#">Blackjack Strategy</a></li>
            <li><a href="#">How to Play Blackjack</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Poker</h3>
          <ul class="mb-0">
            <li><a href="#">Poker Freeroll Tournaments</a></li>
            <li><a href="#">Poker Guide</a></li>
            <li><a href="#">How to Play Poker</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Poker</h3>
          <ul class="mb-0">
            <li><a href="#">Free Video Poker Game</a></li>
            <li><a href="#">Video Poker Online Guide</a></li>
            <li><a href="#">Video Poker Strategy</a></li>
            <li><a href="#">How to Play Video Poker</a></li>
            <li><a href="#">Video Poker Tips</a></li>
            <li><a href="#">Mobile Video Poker</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Baccarat Games</h3>
          <ul class="mb-0">
            <li><a href="#">Baccarat Guide</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Craps Games</h3>
          <ul class="mb-0">
            <li><a href="#">Keno Guide</a></li>
            <li><a href="#">Keno How To Play</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Caribbean Stud Poker</h3>
          <ul class="mb-0">
            <li><a href="#">Caribbean Stud Poker Guide</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Country Guides</h3>
          <ul class="mb-0">
            <li><a href="#">العَرَبِية</a></li>
            <li><a href="#">Albania</a></li>
            <li><a href="#">الجزائر</a></li>
            <li><a href="#">Argentina</a></li>
            <li><a href="#">Armenia</a></li>
            <li><a href="#">Australia</a></li>
            <li><a href="#">Australia Gambling Guide</a></li>
            <li><a href="#">Azerbaijan</a></li>
            <li><a href="#">Bahamas</a></li>
            <li><a href="#">البحرين</a></li>
            <li><a href="#">Bangla</a></li>
            <li><a href="#">বাংলাদেশ</a></li>
            <li><a href="#">Basque</a></li>
            <li><a href="#">Belarus</a></li>
            <li><a href="#">Belgique</a></li>
            <li><a href="#">België</a></li>
            <li><a href="#">Bermuda</a></li>
            <li><a href="#">Bolivia</a></li>
            <li><a href="#">Brasil</a></li>
            <li><a href="#">Brunei</a></li>
            <li><a href="#">Bŭlgariya</a></li>
            <li><a href="#">Cameroun</a></li>
            <li><a href="#">Canada</a></li>
            <li><a href="#">中国</a></li>
            <li><a href="#">Canada Gambling Guide</a></li>
            <li><a href="#">El Salvador</a></li>
            <li><a href="#">Ethiopia</a></li>
            <li><a href="#">Casinos en ligne Canada</a></li>
            <li><a href="#">مصر</a></li>
            <li><a href="#">Catalunya</a></li>
            <li><a href="#">Québec</a></li>
            <li><a href="#">Česká republika</a></li>
            <li><a href="#">Chile</a></li>
            <li><a href="#">Cote D'Ivoire</a></li>
            <li><a href="#">Colombia</a></li>
            <li><a href="#">Costa Rica</a></li>
            <li><a href="#">Croatia</a></li>
            <li><a href="#">Danmark</a></li>
            <li><a href="#">Deutschland</a></li>
            <li><a href="#">Ecuador</a></li>
            <li><a href="#">España</a></li>
            <li><a href="#">Ελλάδα</a></li>
            <li><a href="#">Georgia</a></li>
            <li><a href="#">Ghana</a></li>
            <li><a href="#">Guatemala</a></li>
            <li><a href="#">Hindi</a></li>
            <li><a href="#">Honduras</a></li>
            <li><a href="#">Hungary</a></li>
            <li><a href="#">India</a></li>
            <li><a href="#">ɪndonesia</a></li>
            <li><a href="#">Ireland</a></li>
            <li><a href="#">Íslandi</a></li>
            <li><a href="#">Italia</a></li>
            <li><a href="#">الأردن</a></li>
            <li><a href="#">日本</a></li>
            <li><a href="#">Қазақстан</a></li>
            <li><a href="#">Kenya</a></li>
            <li><a href="#">الكويت</a></li>
            <li><a href="#">Kuwait(EN)</a></li>
            <li><a href="#">Kıbrıs</a></li>
            <li><a href="#">Κύπρος</a></li>
            <li><a href="#">Latvija</a></li>
            <li><a href="#">لبنان</a></li>
            <li><a href="#">Liberia</a></li>
            <li><a href="#">Liechtenstein</a></li>
            <li><a href="#">Lietuviški</a></li>
            <li><a href="#">Luxembourg</a></li>
            <li><a href="#">Luxemburg</a></li>
            <li><a href="#">Macau</a></li>
            <li><a href="#">Macedonia</a></li>
            <li><a href="#">Malaysia</a></li>
            <li><a href="#">Malta</a></li>
            <li><a href="#">México</a></li>
            <li><a href="#">Moldova</a></li>
            <li><a href="#">المغرب</a></li>
            <li><a href="#">မြန်မာ</a></li>
            <li><a href="#">Nepal</a></li>
            <li><a href="#">New Zealand</a></li>
            <li><a href="#">New Zealand Gambling Guide</a></li>
            <li><a href="#">Nicaragua</a></li>
            <li><a href="#">Nigeria</a></li>
            <li><a href="#">Norge</a></li>
            <li><a href="#">Öesterreich</a></li>
            <li><a href="#">عمان</a></li>
            <li><a href="#">Pakistan</a></li>
            <li><a href="#">Panama</a></li>
            <li><a href="#">Paraguay</a></li>
            <li><a href="#">Perú</a></li>
            <li><a href="#">Pilipinas</a></li>
            <li><a href="#">Puerto Rico</a></li>
            <li><a href="#">Philippines</a></li>
            <li><a href="#">Pilipinas</a></li>
            <li><a href="#">Polska</a></li>
            <li><a href="#">Puerto Rico</a></li>
            <li><a href="#">Português</a></li>
            <li><a href="#">قطر‎</a></li>
            <li><a href="#">República Dominicana</a></li>
            <li><a href="#">Romania</a></li>
            <li><a href="#">Serbia</a></li>
            <li><a href="#">Senegal</a></li>
            <li><a href="#">Seychelles</a></li>
            <li><a href="#">Schweiz</a></li>
            <li><a href="#">Singapore</a></li>
            <li><a href="#">Slovakia</a></li>
            <li><a href="#">Slovenia</a></li>
            <li><a href="#">South Africa</a></li>
            <li><a href="#">South Africa Gambling Guide</a></li>
            <li><a href="#">South Korea</a></li>
            <li><a href="#">Sri Lanka</a></li>
            <li><a href="#">Suisse</a></li>
            <li><a href="#">Svizzera</a></li>
            <li><a href="#">ไทย</a></li>
            <li><a href="#">تونس</a></li>
            <li><a href="#">Suomi</a></li>
            <li><a href="#">Sverige</a></li>
            <li><a href="#">Тоҷикистон</a></li>
            <li><a href="#">Trinidad & Tobago</a></li>
            <li><a href="#">Türkiye</a></li>
            <li><a href="#">Uganda</a></li>
            <li><a href="#">Uruguay</a></li>
            <li><a href="#">UK</a></li>
            <li><a href="#">Ukraine</a></li>
            <li><a href="#">الإمارات العربية المتحدة</a></li>
            <li><a href="#">United Arab Emirates(EN)</a></li>
            <li><a href="#">Uzbekistan</a></li>
            <li><a href="#">Venezuela</a></li>
            <li><a href="#">Việt Nam</a></li>
            <li><a href="#">Zambia</a></li>
            <li><a href="#">Zimbabwe</a></li>
          </ul>
        </div>

      </div>

      <div class="col-12 col-sm-4">
        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Site Info</h3>
          <ul class="mb-0">
            <li><a href="#">About Casino.org</a></li>
            <li><a href="#">Seal Certificate</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Casino.org Blog</a></li>
            <li><a href="#">Casino FAQ</a></li>
            <li><a href="#">Casino.org News</a></li>
            <li><a href="#">Responsible Gambling</a></li>
            <li><a href="#">Terms of Service</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Editorial Guidelines</a></li>
            <li><a href="#">Get Listed</a></li>
            <li><a href="#">Meet the team</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">How we rate</a></li>
            <li><a href="#">Casino.org membership</a></li>
            <li><a href="#">Media Center</a></li>
            <li><a href="#">Features Center</a></li>
            <li><a href="#">COVID-19 and Responsible Gambling</a></li>
            <li><a href="#">Safe and Secure</a></li>
            <li><a href="#">World</a></li>
            <li><a href="#">Online casino complaints</a></li>
            <li><a href="#">Casino.org Competitions</a></li>
            <li><a href="#">Casino.org Celebration</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Casino Software and Sites</h3>
          <ul class="mb-0">
            <li><a href="#">Online Casino Reviews</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Online Casinos</h3>
          <ul class="mb-0">
            <li><a href="#">10bet Sports</a></li>
            <li><a href="#">123 Spins</a></li>
            <li><a href="#">22bet</a></li>
            <li><a href="#">22bet Sports</a></li>
            <li><a href="#">32Red</a></li>
            <li><a href="#">32red Mobile</a></li>
            <li><a href="#">32red Sports</a></li>
            <li><a href="#">7 Sultans</a></li>
            <li><a href="#">7 Casino</a></li>
            <li><a href="#">777 Casino</a></li>
            <li><a href="#">Ikibu</a></li>
            <li><a href="#">888 Casino</a></li>
            <li><a href="#">888 Casino Mobile</a></li>
            <li><a href="#">Ace Kingdom</a></li>
            <li><a href="#">Ahti Games</a></li>
            <li><a href="#">Aladdin Slots Casino</a></li>
            <li><a href="#">All British</a></li>
            <li><a href="#">All Slots</a></li>
            <li><a href="#">Amazon Slots</a></li>
            <li><a href="#">Avenger Slots</a></li>
            <li><a href="#">Bet at Home</a></li>
            <li><a href="#">Betamo Casino</a></li>
            <li><a href="#">Betfair Casino</a></li>
            <li><a href="#">Bet Easy</a></li>
            <li><a href="#">Betfair Sports</a></li>
            <li><a href="#">Betfred</a></li>
            <li><a href="#">Betfred Sports</a></li>
            <li><a href="#">Bethard</a></li>
            <li><a href="#">Bethard Sports</a></li>
            <li><a href="#">Betsafe</a></li>
            <li><a href="#">Betsafe Sports</a></li>
            <li><a href="#">Betonline Sports</a></li>
            <li><a href="#">Betspin</a></li>
            <li><a href="#">Betsson</a></li>
            <li><a href="#">BetStars</a></li>
            <li><a href="#">Betvictor</a></li>
            <li><a href="#">Betvictor Sports</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Australia State Guides</h3>
          <ul class="mb-0">
            <li><a href="#">Capital Territory</a></li>
            <li><a href="#">New South Wales</a></li>
            <li><a href="#">Northern Territory</a></li>
            <li><a href="#">Queensland</a></li>
            <li><a href="#">South Australia</a></li>
            <li><a href="#">Tasmania</a></li>
            <li><a href="#">Victoria</a></li>
            <li><a href="#">Western Australia</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Canada State Guides</h3>
          <ul class="mb-0">
            <li><a href="#">Alberta</a></li>
            <li><a href="#">British Columbia</a></li>
            <li><a href="#">Calgary</a></li>
            <li><a href="#">Montreal</a></li>
            <li><a href="#">Ontario</a></li>
            <li><a href="#">Ottawa</a></li>
            <li><a href="#">Toronto</a></li>
            <li><a href="#">Vancouver</a></li>
          </ul>
        </div>

        <div class="sites_wrap">
          <h3 class="hd-3 drop_tick">Land-Based Casino Guides</h3>
          <ul class="mb-0">
            <li><a href="#">Australian Local Casinos</a></li>
            <li><a href="#">Brazil Royal Panda</a></li>
            <li><a href="#">Canadian Local Casinos</a></li>
            <li><a href="#">Macau</a></li>
            <li><a href="#">Sands Macao</a></li>
            <li><a href="#">Galaxy Rio Macau</a></li>
            <li><a href="#">Grand Lisboa</a></li>
            <li><a href="#">Venetian Macao</a></li>
            <li><a href="#">Wynn Macau</a></li>
            <li><a href="#">New Zealand Local Casinos</a></li>
            <li><a href="#">South Africa Local Casinos</a></li>
            <li><a href="#">UK Local Casinos</a></li>
            <li><a href="#">US Local Casinos</a></li>
            <li><a href="#">US Gambling Guide</a></li>
            <li><a href="#">Aspers</a></li>
            <li><a href="#">Palm Beach</a></li>
            <li><a href="#">Empire</a></li>
            <li><a href="#">Aspinalls</a></li>
            <li><a href="#">Les Ambassadeurs</a></li>
            <li><a href="#">Crockfords</a></li>
            <li><a href="#">Ritz</a></li>
            <li><a href="#">Victoria</a></li>
          </ul>
        </div>


      </div>
    </div>
  </div>
</div>
@endsection