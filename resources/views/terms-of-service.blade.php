@extends('layouts.app')
@section('title') Privacy @endsection
@section('content')
<div class="privacy">
  <div class="container">
    <div class="row">
      <div class="col-12 hf_bar">
        <h2>Terms Of Service</h2>
      </div>
      <p>Welcome to Casino.org (the “Site”).</p>

      <p>The Site is operated by CG Holdings Ltd (the “Company”, “we” “our” or “us”).</p>

      <p>In addition to these Terms of Service, the Privacy Policy which can be found here (the “Privacy Policy”)
        applies to your use of the Site and the Services (as defined below). The Privacy Policy is incorporated by
        reference into the Terms of Service and forms an integral part thereof. The Terms of Service and the Privacy
        Policy shall collectively be referred to as the “Agreement” which constitutes a binding legal agreement between
        you and us as well as governing the relationship between you and us.</p>

      <p>In the Agreement, "you" or "your" or "user" or “player” means any person who uses the Site, Services or the
        Software under the Agreement.</p>

      <p>Important: Please review the Privacy Policy prior to your use of the Site or the Services.</p>

      <p>By using or accessing the Site or, you consent to the terms and conditions set forth in the Agreement and agree
        to be bound by its terms.</p>

    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">Acceptance and Amendment of the Agreement</h3>
      <p>The first type of information is un-identified and non-identifiable information pertaining to a user(s), which
        may be made available or gathered via your use of the Services and interaction with the website (“Non-personal
        Information”). We are not able to identify the user from the Non-personal Information that is collected.</p>

      <p>In order to enhance the functionality of the Services and to provide you with a better user experience, we
        collect technical information transmitted by your computer (through the use of third party analytical cookies),
        including certain software and hardware information (e.g. the type of browser and operating system your device
        uses, language preference, access time and the domain name of the website from which you linked to the Services;
        etc.).</p>

      <p>We also collect information about your use of the Services, such as user activity (e.g. pages viewed, the
        amount of time spent on particular pages, online browsing, clicks, actions, etc.). This information is
        collected, amongst other things, for research and analytics purposes about your use of the Services.</p>

      <p>We aggregate, anonymize or de-identify the information collected by the Services or via other means so that the
        information cannot, on its own, personally identify you. Our use and disclosure of such aggregated or
        de-identified information is not subject to any restrictions under this Privacy Policy, and we may disclose it
        to others without limitation and for any purpose.</p>

      <p>The second type of information is individually identifiable information, namely information that identifies an
        individual or may with reasonable effort identify an individual (“Personal Information”). This may be collected
        when you subscribe for our email newsletters by providing us with your email address on the website or when you
        post a comment including your name and email address in response to one of our news pieces.</p>

      <p>We may also collect some of your Personal Information if you use our “Live Chat” feature on the website,
        depending on the level of information you provide and the nature of your query. We may also collect information
        such as your name, email address and interests/preferences if you register on our membership page and submit
        your preferences.</p>

      <p>We may also collect specific types of connection details and information with regard to your device and
        hardware, such as IP addresses.</p>

      <p>If we combine Personal Information with Non-personal Information, the combined information will be treated as
        Personal Information for as long as it remains combined.</p>
      <ul class="yellow_arrowlist">
        <li>If you do agree to any term or conditions of the Agreement you should immediately stop using the Site and
          the Services.</li>

        <li>We may amend the Agreement from time to time and any changes made shall come into effect 14 days after being
          published on the Site or earlier if required by any applicable law, regulation or directive. You agree that
          your access or use of the Site or your use of the Services following such period will be deemed to constitute
          your acceptance of the amendments made to the Agreement.</li>

        <li>It remains your responsibility to ensure that you are aware of the correct, current terms and conditions of
          Agreement and we advise you to check the Terms of Service and the Privacy Policy on a regular basis.</li>

        <li>We may terminate or suspend your use of the Services and/or this Site at any time, at our sole discretion
          and for any reason which may include but is not limited to a breach by you of the Agreement without providing
          any financial compensation to you.</li>
      </ul>
    </div>
    <hr>


    <div class="my-4">
      <h3 class="hd-3 green_tick">Use of the Site and Services</h3>
      <ul class="yellow_arrowlist">
        <li>The Site and the Services may only be used by you if you are over the age of 18 and over the age for which
          the Site and the Services are legal under the laws of any jurisdiction which applies to you (the "Legal Age").
        </li>

        <li>The Site and the materials incorporated therein are not designed to appeal or target those who have not yet
          reached Legal Age.</li>

        <li>If you are not of Legal Age you must immediately stop using or accessing the Site and the Services.</li>
      </ul>
    </div>
    <hr>


    <div class="my-4">
      <h3 class="hd-3 green_tick">The Services</h3>
      <ul class="yellow_arrowlist">
        <li>The Site provides information with regards to casino games and the gambling industry (the "Services" ).</li>

        <li>The Site and the Services are provided for free and for informational purposes only.</li>

        <li>The Company does not operate any online casino or poker website nor does it accept any bets or wagers.</li>
      </ul>
    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">Intellectual Property Rights</h3>
      <ul class="yellow_arrowlist">
        <li>The Company, its affiliates and its licensors (as applicable) own all software, data, written materials and
          other content, graphics, forms, artwork, images, pictures, graphics, photographs, functional components,
          animations, videos, music, audio, text and any software concepts and documentation and other material on, in
          or made available through the Site (collectively the “Site Content”).</li>

        <li>You agree not to remove or alter any copyright notice or any other proprietary notice on the Site or the
          Site Content.</li>

        <li>In addition, the brand names and any other trademarks, service marks and/or trade names used on this Site
          (the “Trade Marks”) are the trademarks, service marks and/or trade names of the Company, its affiliates or its
          licensors (as applicable) and these entities reserve all rights to such Trade Marks.</li>

        <li>The Site Content and Trademarks are protected by copyright and/or other intellectual property rights. You
          hereby acknowledge that by using the Services or by using or visiting the Site, you obtain no rights in the
          Trade Marks and the Site Content and you may only use the same in complete accordance with the Agreement.</li>
      </ul>
    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">User Content</h3>
      <ul class="yellow_arrowlist">
        <li>We may permit you to transmit, upload, post, e-mail or otherwise make available data, text, software, music,
          sound, photographs, graphics, images, videos, messages or other materials ("User Content") on the Site, which
          may include but is not limited to via online discussion forums and chat facilities.</li>

        <li>You are entirely responsible for such User Content and the Company and its affiliates shall have no
          liability to you with respect of the User Content and you hereby waive all claims against us and our
          affiliates in this regard.</li>

        <li>By providing the User Content you grant the Company and its affiliates a, perpetual, irrevocable,
          transferable, worldwide license to use, copy, perform exploit, distribute, reproduce, display, modify, add to,
          subtract from, translate, edit and create derivative works based upon the User Content or any portion thereof
          in any manner (including without limitation promotional and advertising purposes) and in any and all media now
          known or hereafter devised all without any compensation to you whatsoever. You also agree to waive all moral
          rights to the User Content.</li>

        <li>You acknowledge and agree that neither the Company nor its affiliates is obligated to monitor or review User
          Content.</li>
        <li>You acknowledge and agree that any User Generated Content may be edited or removed by the Company and its
          affiliates and you hereby waive any rights you may have if the User Content is altered or changed.</li>
        <li>When publishing or submitting User Content, any personally identifiable information that you submit, can be
          read, collected, or used by other visitors or users of the Site and can be used by third parties to for
          example, send you unsolicited messages. The Company and its affiliates are not and shall not be responsible
          for the personally identifiable information that you choose to submit in the User Content.</li>
        <li>You shall at all times be polite when interacting with other users or visitors of the Site and you shall not
          to engage in any behavior that may be construed by us as aggressive, harassing, tortious, defamatory,
          libelous, vulgar, hateful, obscene, offensive, racist, sexist, insulting or otherwise inappropriate towards
          other users.</li>
        <li>You agree not to engage in or assist or encourage others to engage in transmitting, uploading, posting, or
          otherwise making available on the Site, User Content or any other content that: (i) is, or which encourages
          activity or conduct that is unlawful, harmful, threatening, abusive, harassing, tortious, defamatory,
          insulting, vulgar, obscene, pornographic, libelous, invasive of another's privacy, hateful, or racially,
          ethnically or otherwise objectionable; (ii) you do not have a right to make available under any law or under
          contractual or fiduciary relationships; (iii) infringes any proprietary right of any third party which
          includes but is not limited to intellectual property rights of any person or entity or any rights of
          publicity, personality, or privacy of any person or entity including as a result of your failure to obtain
          consent to post personally identifying or otherwise private information about a person; (iv) you were
          compensated for or granted any consideration of any nature by any third party; (v) contains restricted or
          hidden content; (vi) violates any applicable law, statute, ordinance, regulation, or agreement; (vii) is
          untrue, malicious or which is damaging to the Company, its affiliates or the Site; (viii) is designed be
          designed to interfere or interrupt the Site; (ix) infect the Site with a virus or other computer code, files
          or programs that are designed to interrupt, destroy or limit the functionality of any computer software or
          hardware; and (x) advertises, promotes or otherwise relates to any other online entities or sites which are
          competitors of the Company, its affiliates or the Site.</li>
        <li>You shall not misrepresent or make false statements regarding the source or origin of any User Content.</li>
      </ul>
    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">Prohibited Activities</h3>
      <ul class="yellow_arrowlist">
        <li>By visiting or using using the Site you agree not to and not to permit others:</li>
      </ul>
      <p>a. access or collect any personally identifiable information of other users or visitors of the Site for any
        reason whatsoever;<br>
        b. use the Site, the Services, the Site Content in connection with any unlawful activity;<br>
        c. copy, redistribute, publish, reverse engineer, decompile, disassemble, modify, translate or make any attempt
        to access the source code to create derivate works of the source code, or otherwise;<br>
        d. to harvest or collect any data or information through the Site, or use any robot, spider, scraper or any
        other means, automated or otherwise, to access the Site;<br>
        e. disclose any data about the Site or the Services to any third parties;<br>
        f. distribute any malicious code viruses, spyware, trojans, worms, spybots, keyloggers or any other form of
        malware, droppers, logic bombs, hidden files, locks, clocks, copy protection features, CPU serial number
        references or any other device of similar intent to the Site or Services or upload any upload files designed to
        harm the Site, the Services or the users or visitors to the Site or users of the Services;<br>
        g. not modify, lease, copy, distribute, display, perform, reproduce, publish, licence, create derivative works
        from, transfer, or sell any of the Site Content, Trademarks or User Content;<br>
        h. make the software available to any third party through a computer network or otherwise;<br>
        I. not to take any action that would reduce or harm the Company’s, its affiliates or the Sites goodwill or
        reputation. </p>
    </div>
    <hr>


    <div class="my-4">
      <h3 class="hd-3 green_tick">Third-Party Content</h3>
      <ul class="yellow_arrowlist">
        <li>This Site may contain hyperlinks to other websites, services or products or content operated by
          persons/entities other than us (collectively “Third Party Content”). Such hyperlinks are provided for your
          reference and convenience only. You agree not to hold us responsible for the Third Party Content . A hyperlink
          from this Site to the Third Party Content does not imply that we endorse such Third Party Content. You are
          solely responsible for determining the extent to which you may use any Third Party Content and do so at your
          own risk.</li>
        <li>We do not endorse nor do we make any warranties, representations with respect to any such to the Third Party
          Content (which includes but is not limited to the accuracy of the information, the quality of products or
          services contained in the Third Party Content).</li>
      </ul>
    </div>
    <hr>


    <div class="my-4">
      <h3 class="hd-3 green_tick">Gaming Services</h3>
      <ul class="yellow_arrowlist">
        <li>The Site, Site Content and Services makes available information for your personal entertainment and
          informational purposes only.</li>
        <li>The Site, Site Content may contain references to, link to or advertise Third Party Content which relates to
          online gaming and gambling services (the “Gaming Services”).</li>
        <li>The Gaming Services are only directed to and are intended to be viewed and used for those users or visitors
          to the Site who are located in jurisdictions where the use of the Gaming Services is legal.</li>
        <li>Without limiting the foregoing, you understand that laws regarding online gaming and gambling vary
          throughout the world, and it is your sole obligation to ensure that you fully comply with any law, regulation
          or directive, applicable to the country you are located in with regards to the use of the Site, Services and
          the Gaming Services. The ability to access to the Site does not necessarily mean that the Site, the Services,
          the Site Content, Gaming Services and/or your activities via the Site, are legal under the laws, regulations
          or directives applicable to the country you are located in.</li>
      </ul>
    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">Disclaimer</h3>
      <ul class="yellow_arrowlist">
        <li>YOUR ACCESS TO AND USE OF THE SITE, THE SERVICES AND ALL MATERIALS ON THE SITE OR MADE AVAILABLE VIA THE
          SERVICES IS AT YOUR SOLE OPTION, DISCRETION AND RISK.</li>
        <li>THE SITE, THE SERVICES AND ALL MATERIALS ON THE SITE OR MADE AVAILABLE VIA THE SERVICES ARE MADE AVIALABLE
          ON AN "AS IS" BASIS. THE COMPANY, ITS AFFILIATES AND THEIR LICENSORS DISCLAIM WITH REGARDS TO THE SITE, THE
          SERVICES AND ALL MATERIALS ON THE SITE OR MADE AVAILABLE VIA THE SERVICES ALL EXPRESS OR IMPLIED CONDITIONS,
          REPRESENTATIONS, AND WARRANTIES (WHETHER BY LAW, STATUTE OR OTHERWISE) INCLUDING, WITHOUT LIMITATION, ANY
          IMPLIED WARRANTY OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT,
          SATISFACTORY QUALITY, NON-INTERFERENCE, ACCURACY OF THE SITE OR THE SERVICES, OR INFRINGEMENT OF APPLICABLE
          LAWS AND REGULATIONS.</li>
        <li>THE COMPANY MAKES NO WARRANTY THAT THE SITE, THE SERVICES AND ALL MATERIALS ON THE SITE OR MADE AVAILABLE
          VIA THE SERVICES WILL MEET YOUR REQUIREMENTS, BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE, THAT DEFECTS
          WILL BE CORRECTED, OR THAT THEY ARE FREE OF VIRUSES SPYWARE, MALWARE OR BUGS.</li>
      </ul>
    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">Limitation of Liability</h3>
      <ul class="yellow_arrowlist">
        <li>The Company, its affiliates and their licensors shall not be liable to you or any third party in contract,
          tort, negligence, or otherwise, for any loss or damage whatsoever arising from or in any way connected with
          your, or any third party's, use or access of the Site or the Services, whether direct or indirect, including,
          without limitation, damage for loss of business, loss of profits (including loss of or failure to receive
          anticipated winnings), business interruption, loss of business information, or any other pecuniary or
          consequential loss (even where we have been notified by you of the possibility of such loss or damage).</li>
        <li>The Company, its affiliates and licensors shall not be liable in contract, tort or otherwise, for any loss
          or damage whatsoever arising from or in any way connected with your use, of any link contained on the Site nor
          are they responsible for the content contained on any Internet site linked to from the Site.</li>
        <li>You confirm that the Company shall not be liable to you or any third party for any modification to,
          suspension of or discontinuance of the Site or the Services.</li>
      </ul>
    </div>
    <hr>


    <div class="my-4">
      <h3 class="hd-3 green_tick">Indemnity</h3>
      <ul class="yellow_arrowlist">
        <li>By visiting or using the Site or by using the Services, you agree to fully indemnify, defend and hold us,
          and our officers, directors, employees, agents, licensors, suppliers, harmless (collectively the “Indemnified
          Parties”) immediately on demand, from and against and all claims, liabilities, proceedings, damages, losses,
          liabilities, fines costs and expenses of any kind which includes but is not limited to legal fees, arising out
          of or incurred as a result of: (i) any breach of the Agreement; (ii) your access and use of the Site or the
          Services (or by anyone else using your username and password); (iii) your violation of any law; (iv) your
          negligence; (v) your willful misconduct (collectively the “Claims”).</li>
        <li>You hereby agree: (i) to immediately notify us of any Claim; (ii) not to settle any Claim without our prior
          written consent; (iii) that the Indemnified Parties (as applicable) may assume the defense of any claim and
          you shall co-operate to all reasonable requests for information and assistance with respect to the Claims.
        </li>
        <li>You shall have the right to employ separate counsel of any Claim and to participate in the defense thereof.
        </li>
        <li>In the event that the Indemnified Parties (as applicable) do not notify you that we elect to undertake the
          defense of the Claim, you shall have the right to defend the Claim with counsel reasonably acceptable to the
          Indemnified Party, subject to the applicable Indemnified Parties right to assume, at their sole cost and
          expense, the defense of any Claim at any time prior to the settlement or final determination thereof.</li>
      </ul>
    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">Notification of Copyright Infringement</h3>
      <ul class="yellow_arrowlist">
        <li>We respect the intellectual property rights of others and we require users and visitors of the Site and the
          users of the Services to do the same. If you believe any User Content on the Site infringes upon a copyright,
          or otherwise violates your intellectual property rights, you should notify the Company’s Copyright Agent by
          providing the following information:</li>
      </ul>
      <p>a. Your name and the name of your company or Subscribing Organization, if applicable;<br>
        b. Your contact information which must include your full email address, physical address and telephone
        number;<br>
        c. Identify the material on the Site that may be an infringement with enough detail so that we may locate it on
        the Site. You should include the the URL that points to the allegedly copyright infringing content include or a
        detailed description of where to find the allegedly copyright infringing content ;<br>
        d. A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right
        that is allegedly infringed;
        e. A statement that the complaining party has a good faith belief that use of the material in the manner
        complained of is not authorized by the copyright owner, its agent, or the law; and<br>
        f. A statement that the information in the notification is accurate, and under penalty of perjury, that the
        complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.
        <br>
      </p>

      <p>Our Copyright Agent may be reached at the following contact information: casino@casino.org</p>
    </div>
    <hr>


    <div class="my-4">
      <h3 class="hd-3 green_tick">Termination of the Agreement</h3>
      <ul class="yellow_arrowlist">
        <li>We may terminate the Agreement as well as terminate your access to the Site and the Services immediately
          without notice to you (and without any financial compensation to you):</li>
      </ul>
      <p>a. if for any reason we decide to discontinue to provide the Services or the Site or any part thereof, in
        general or specifically to you;<br>
        b. if we believe that you have breached any of the terms of the Agreement;<br>
        c. if your use of the Services or the Site has been in any way improper or breaches the spirit of the Agreement;
        or<br>
        d. for any other reasonable grounds we see fit. </p>
    </div>
    <hr>


    <div class="my-4">
      <h3 class="hd-3 green_tick">Governing Law and Arbitration</h3>
      <ul class="yellow_arrowlist">
        <li>By visiting or using the Site or the Services, you agree that the Agreement and your use of the Site and the
          Services shall be governed exclusively by the laws of England & Wales, without regard to its conflict of law
          provisions.</li>
        <li>In the event of any controversy, claim, or dispute arising out of or relating to the Agreement, your use of
          the Site or the Services (“Dispute”), the parties hereto shall consult and negotiate with each other and,
          recognizing their mutual interests, attempt to reach a satisfactory solution. In the event the parties do not
          resolve or settle the dispute within a period of thirty (30) days of beginning any such consultation or
          negotiation, then upon notice by any party to the other, any unresolved Dispute, including but not limited to
          any question regarding the Agreement’s existence, validity or termination, shall be referred to and finally
          resolved by binding arbitration under the London Court of International Arbitration (“LCIA” ) Rules
          (“Arbitration”), which Rules are deemed to be incorporated by reference into this clause. It is agreed that:
        </li>
      </ul>
      <p>a. The number of arbitrators shall be one;<br>
        b. The seat, or legal place, of arbitration shall London, United Kingdom. The language to be used in the
        arbitral proceeding shall be English;<br>
        c. the appointing authority for the purposes of the Rules shall be the London Court of International
        Arbitration;<br>
        d. the seat, or legal place, of the arbitration shall be London;<br>
        e. the language to be used in the arbitration shall be English; and<br>
        f. the governing law of this arbitration agreement shall the substantive law of England and Wales.</p>
    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">Confidentiality</h3>
      <p>By visiting or using this Site or by using the Services, you agree that, except as directed by us, you shall
        keep strictly confidential and shall not disclose, during the term of the Agreement or any time thereafter, the
        existence of any Dispute or the subject matter of any Dispute other than to LCIA in connection with resolving
        the Dispute, any or all of the information disclosed at Arbitration, or the results of Arbitration
        (collectively, the “Confidential Information”) to any person or entity, nor will You use or exploit, directly or
        indirectly, the Confidential Information for any purpose other than to resolve the Dispute in strict confidence,
        to participant in the Arbitration, or to give effect to the result of the Arbitration.</p>

      <p>Notwithstanding the foregoing, you will be entitled to disclose such Confidential Information if required by
        law provided that You promptly notify us, consult with us and cooperate with us in any attempt to resist or
        narrow such disclosure or to obtain an order or other assurance that such Confidential Information will be
        accorded confidential treatment.</p>
    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">Entire Agreement</h3>
      <p>The Agreement contains the entire agreement between us and you relating to your use of the Site, the Software
        and the Services and supersedes any and all prior agreement between us and you in relation to the same. You
        confirm that, in agreeing to accept the Agreement, you have not relied on any warranty or representation save
        insofar as the same has expressly been made a representation by the Company in the Agreement.</p>
    </div>
    <hr>


    <div class="my-4">
      <h3 class="hd-3 green_tick">Severability</h3>
      <p>To the extent permitted by law, all provisions of this Agreement shall be severable and no provision shall be
        affected by the invalidity of any other provision.</p>
    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">Irreparable Harm</h3>
      <p>You acknowledge and agree that your breach of any of the Agreement could cause irreparable harm to us. Without
        affecting any other rights and remedies that we may have and despite anything to the contrary in this Agreement,
        you hereby acknowledge and agree that damages would not be an adequate remedy for any breach by you of the
        provisions of this Agreement, and that the we shall be entitled to remedies of injunction, specific performance
        and other equitable relief for any threatened or actual breach of the provisions of this Agreement and that no
        proof of special damages shall be necessary for the enforcement of this Agreement.</p>
    </div>
    <hr>


    <div class="my-4">
      <h3 class="hd-3 green_tick">Surviving Provisions</h3>
      <p>Any provisions hereof which expressly or by their nature are required to survive termination or expiration of
        this Agreement in order to achieve their purpose shall so survive until it shall no longer be necessary for them
        to survive in order to achieve that purpose. Without derogating from the generality of the foregoing, Sections
        4, 6, 7 and 10-22 (inclusive) hereof shall survive termination of this Agreement.</p>
    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">Waiver</h3>
      <p>No waiver by us of any terms of the Agreement shall be construed as a waiver of any preceding or succeeding
        breach of any terms of the Agreement.</p>
    </div>
    <hr>

    <div class="my-4">
      <h3 class="hd-3 green_tick">Third Parties</h3>
      <p>Unless otherwise expressly stated, nothing in this Agreement shall create or confer any rights or any other
        benefits to third parties. Nothing in the Agreement shall be construed as creating any agency, partnership,
        trust arrangement, fiduciary relationship or any other form of joint enterprise between you and us.</p>

      <p>Nothing in the Agreement shall be construed as creating any agency, partnership, trust arrangement, fiduciary
        relationship or any other form of joint enterprise between you and us.</p>
    </div>
    <hr>


    <div class="my-4">
      <h3 class="hd-3 green_tick">Assignment</h3>
      <p>We reserve the right to transfer, assign, sublicense or pledge the Agreement, in whole or in part, without your
        consent: (i) to any entity within the same corporate group as the Company, or (ii) in the event of a merger,
        sale of assets or other similar corporate transaction in which the Company may be involved in. You may not
        transfer, assign, sublicense or pledge in any manner whatsoever any of your rights or obligations under the
        Agreement.</p>
    </div>
    <hr>


    <div class="my-4">
      <h3 class="hd-3 green_tick">usgamblinglist.com Competitions 2020</h3>
      <p>We reserve the right to transfer, assign, sublicense or pledge the Agreement, in whole or in part, without your
        consent: (i) to any entity within the same corporate group as the Company, or (ii) in the event of a merger,
        sale of assets or other similar corporate transaction in which the Company may be involved in. You may not
        transfer, assign, sublicense or pledge in any manner whatsoever any of your rights or obligations under the
        Agreement.</p>
      <ul class="yellow_arrowlist">
        <li>Eligibility: This Competition is open to residents of permitted countries (and/or states) only aged 18 or
          over, excluding employees of any entity that owns or operates Casino.org (“the Promoter”), their families, or
          agents, businesses or anyone else professionally connected with this Competition. <br><br>

          Participation in this Competition may be illegal under the laws of certain jurisdictions and that the operator
          of the site makes no representation and bears no liability in connection with the legality of participation
          for the individual user and reserves its right to prevent access to the Competition by any individual and
          withdraw any prize if it deems, in its absolute discretion, it necessary to do so. Each user must satisfy
          themselves as to the legality of entry and participation into the Competition and it the individual user's
          sole responsibility to check all local laws that may apply to their entry and participation into the
          Competition.</li>
        <li>Competition Period: Entrance opens from 9.30am GMT on 10th December 2021 (“Opening Date”) and closes at 5pm
          GMT on 6th January 2021 (“Closing Date”) unless the Promoter gives notice on its website to extend the
          Competition Closing Date. Please note that the current T&Cs also apply to previous Competition promotions
          during period March to November 2021.</li>
        <li>No purchase necessary: Entry is made online only via the casino.org website, internet access is required.
        </li>
        <li>To Enter: To enter the main prize draw, play any free to play games on the membership section of the website
          (including Wheel of Wonder, Lucky Match and Snake Nostalgia), or carry out another activity listed in the
          competition area. To win an instant prize, play Wheel of Wonder and Fortune Spin. Entries can only be gained
          by completing these activities via the membership section.</li>
        <li>Competition Prizes: The Competition prizes are as follows:<br><br>
          1st prize – 1 x $500 Amazon US Voucher - Winner will be drawn at the end of the promotion period and contacted
          via email<br>
          2nd prize – 20 x $100 Amazon US Vouchers available to win instantly (voucher codes issued in our platform)<br>
          <br>
          Winners have to open an Amazon US account if they don't already have one in order to redeem the code
        </li>
        <li>The prizes are non-transferable and non-exchangeable.</li>
        <li>In the event of unforeseen circumstances the Promoter reserves the right to substitute the prizes for an
          alternative of equal or greater value.</li>
        <li>Entry is limited to a per person basis. Any duplicate, 3rd party, or scripted entries may result in
          suspension or disqualification.</li>
        <li>Winner Selection: Those entrants that play/complete the games or carry out other actions as referred to
          above can be entered into a random draw to select the winners of the Competition prizes. Other entrants that
          play/complete the games or carry out other actions as referred to above can win an instant prize.</li>
        <li>Winner Notification: The winner will be contacted within 28 days of the draw, and the winner will be
          required to respond to accept the prize and provide details within 3 days of initial contact. The Promoter
          will make all reasonable efforts to contact the winner via email. If the winner cannot be contacted or is not
          available, the Promoter reserves the right to offer the prize to the next eligible entrant randomly selected
          from the draw.</li>
        <li>Prize delivery: To enable the delivery of prizes, entrants may be required to provide name, email address,
          location and age. The prizes are intended to be sent in the form of email, to the extent applicable.</li>
        <li>Verification: The Promoter reserves the right to verify the eligibility of entrants including age. The
          Promoter may require such information as it considers reasonably necessary for this purpose and a prize may be
          withheld unless and until the Promoter is satisfied with the verification.</li>
        <li>Disqualification: The Promoter reserves the right to disqualify any entrant it reasonably believes is
          attempting to fraudulently enter, cheat, or compromise the system either by technical means (e.g. scripting or
          automation), or manual means (e.g. creation of multiple accounts per entrant).</li>
        <li>No responsibility is accepted for entries lost, damaged or delayed or as a result of any network, computer
          hardware or software failure of any kind. Proof of submitting responses will not be accepted as proof of entry
          into the Competition.</li>
        <li>Data Protection: Entrants agree that, by entering into this Competition, personal data shall be collected
          and processed by the Promoter and stored on the Promoter’s respective database and used for the administration
          of its ongoing relationship with participants and in connection with the Promotion. Entrants may also receive
          marketing communications via email if they have opted in to receive them, and may unsubscribe to these emails
          at any time.<br>
          View the Casino.org Privacy Policy https://www.casino.org/privacy-policy/<br>
          Personal data may also be collected and stored by our business partners involved in the provision of the
          Casino.org Competition games and activities featured on the website.<br>

          It is the responsibility of residents to understand the laws applicable to you in your relevant country,
          state, city and/or town and to comply with the same in relation to the entry into this competition.</li>
        <li>The Promoter, its agents or distributors will not, in any circumstances be responsible or liable to
          compensate the winner or accept any liability for any damage, loss, personal injury or death suffered by any
          entrant entering the Promotion or as a result of accepting or participating in any prize. Nothing shall
          exclude the Promoter’s liability for death or personal injury as a result of its negligence.</li>
        <li>If for any reason the Competition is not capable of running as planned for reasons including but not limited
          to tampering, unauthorised intervention, fraud, technical failures or any other causes beyond the control of
          the Promoter which corrupt or affect the administration, security, fairness, integrity or proper conduct of
          this Competition, the Promoter reserves the right (subject to any written directions given under applicable
          law) to disqualify any individual who tampers with the entry process and to terminate, modify or suspend the
          Competition.</li>
        <li>If an act, omission, event or circumstance occurs which is beyond the reasonable control of the Promoter and
          which prevents the Promoter from complying with these terms and conditions, the Promoter will not be liable
          for any failure to perform or delay in performing its obligation.</li>
        <li>Publicity: If you are the winner of the Competition, you agree that the Promoter may use your name, image,
          town and/or county of residence to announce the winner of this Competition and for any other reasonable and
          related promotional purposes. The winners may also be required to participate in any reasonable publicity
          arising from the Competition. By entering into the Competition, you agree that any personal information
          provided by you with the Competition entry may be held and used only by the Promoter or its agents and
          suppliers to administer the Competition.</li>
        <li>By entering this Competition, all participants will be deemed to have accepted and be bound by these terms
          and conditions. All Competition entries and any accompanying material submitted to the Promoter will become
          the property of the Promoter on receipt.</li>
        <li>The Promoter’s decisions are final and binding in all matters relating to this Competition, and no
          correspondence shall be entered into. If there is any reason to believe that there has been a breach of these
          terms and conditions, the Promoter may, at its sole discretion, reserve the right to exclude you from
          participating in the Competition. The Promoter reserves the right to hold, void, suspend, cancel or amend the
          Competition where it becomes necessary to do so.</li>
        <li>The Terms and Conditions of this Promotion shall be interpreted and applied on the basis of English Law and
          the Courts of England and Wales shall have exclusive jurisdiction</li>
      </ul>
    </div>


  </div>
</div>
@endsection