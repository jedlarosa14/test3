<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

//Review Routes
Route::group(['prefix' => 'review'], function () {
    
    Route::get('bet-online', function () {
        return view('review.bet-online');
    })->name('review.bet-online');

    Route::get('bovada', function () {
        return view('review.bovada');
    })->name('review.bovada');

    Route::get('cafe-casino', function () {
        return view('review.cafe-casino');
    })->name('review.cafe-casino');

    Route::get('ignition-casino', function () {
        return view('review.ignition-casino');
    })->name('review.ignition-casino');

    Route::get('my-bookie', function () {
        return view('review.my-bookie');
    })->name('review.my-bookie');

});

//Guide Routes
Route::group(['prefix' => 'guide'], function () {
    
    Route::get('blackjack-gambling', function () {
        return view('guide.blackjack-gambling');
    })->name('guide.blackjack-gambling');

    Route::get('live-dealer', function () {
        return view('guide.live-dealer');
    })->name('guide.live-dealer');

    Route::get('poker-gambling', function () {
        return view('guide.poker-gambling');
    })->name('guide.poker-gambling');

    Route::get('roulette-gambling', function () {
        return view('guide.roulette-gambling');
    })->name('guide.roulette-gambling');

    Route::get('slots-gambling', function () {
        return view('guide.slots-gambling');
    })->name('guide.slots-gambling');

    Route::get('sports-betting', function () {
        return view('guide.sports-betting');
    })->name('guide.sports-betting');
});


Route::get('privacy', function () {
    return view('privacy');
})->name('privacy');

Route::get('sitemap', function () {
    return view('sitemap');
})->name('sitemap');

Route::get('terms-of-service', function () {
    return view('terms-of-service');
})->name('terms-of-service');


//Need to update below url
Route::get('real-money', function () {
    return view('home');
})->name('real-money');


Route::get('online-casions', function () {
    return view('home');
})->name('online.casions');


//Footer link
Route::get('contact-us',function(){
    return view('home');
})->name('contact-us');

Route::get('how-we-rate',function(){
    return view('home');
})->name('how-we-rate');

Route::get('get-listed',function(){
    return view('home');
})->name('get-listed');

Route::get('membership',function(){
    return view('home');
})->name('membership');

Route::get('careers',function(){
    return view('home');
})->name('careers');

Route::get('get-listed',function(){
    return view('home');
})->name('get-listed');